import os
import discord, asyncio, re
import traceback
import time
from autocorrect import spell
from enchant.checker import SpellChecker
from nlp.main import load_words, print_analogy, print_most_similar
from random import random, choice, randint, sample
from impersonate import build_training_data
from copy import deepcopy
import subprocess
import shutil
import classify_image
import predict_lstm
from e2p import e2p
import signal
import sys



def signal_handler(sig, frame):
    '''Exit on interrupts such as SIGINT and SIGKILL'''
    print('Interrupt or termination detected, exiting.')
    sys.exit(0)

signal.signal(signal.SIGINT, signal_handler)
signal.signal(signal.SIGTERM, signal_handler)


spellchecker = SpellChecker("en_US")
client = discord.Client()
regex = re.compile('[^a-zA-Z \n]')

def load_model_and_corpus():
    with open('new_data/train.to', 'r') as f:
        text = f.read()
    encoded_text = text.encode()
    model = predict_lstm.init(encoded_text)
    corpus = set(text.split())
    return model, corpus

model, corpus = load_model_and_corpus()
print(sample(corpus, 50))

@client.event
async def on_message(message):

    # we do not want the bot to reply to itself
    if message.author == client.user:
        return

    '''for hook in settings.keys():
        if message.content.startswith(hook):
            # Note: no guarding against users with same name as commands, like Anal, in place.
            set_hook_params(hook)
            trunc_content = message.content.replace(hook, "")
            msg = await respond(trunc_content)
            await message.channel.send(msg)
            restore_params()
    '''

    #if message.content.startswith(".e2p") or "513226159530704896" in message.content or (message.channel.id == "494549477407850526" and message.author.id == "313929951114035200" and random() < 0.3):
    if "513226159530704896" in message.content or (message.channel.id == 494549477407850526 and random() < 0.3):
        responded_already = False

        for attachment in message.attachments:
            if attachment.url:
                url = attachment.url
                lurl = url.lower()
                if '.jpg' in lurl or '.jpeg' in lurl or '.png' in lurl:
                    if message.content.startswith(".e2p"):
                        await e2p(url, lambda f: message.channel.send("Here is your porn.", file=discord.File(f)))
                        return
                    else:
                        answers = classify_image.classify_from_url(url)
                        if answers is not None:
                            response = 'That might be a ' + ', '.join(answers) + ', or a '
                            wildcard = "butt."
                            await message.channel.send(response + wildcard)
                            return

        if len(regex.sub('', message.content)) == 0:
            await message.channel.send("I don't fucking know!")
            return
        msg = await respond(message.content, model)
        await message.channel.send(msg)

    if message.content.startswith(".anal") or message.content.startswith(".analogy"):
        message_words = message.content.split()[1:]
        if len(message_words) == 3:
            left2, left1, right2, right1 = print_analogy(
                message_words[0],
                message_words[1],
                message_words[2],
                words
            )
            if right1 == "?":
                message_words = [unemojify(word) for word in message_words]
                left2, left1, right2, right1 = print_analogy(
                    message_words[0],
                    message_words[1],
                    message_words[2],
                    words
                )
            if right1 == "?":
                right1 = find_capitalized_analogy(message_words)
            if right1 == "?":
                right1 = find_spellchecked_analogy(message_words)
            if right1 == "?":
                right1 = find_capitalized_analogy(message_words)

            emojis = list(client.emojis)
            response = process_emojis2(left2 + " is to " + left1 + " as " + right2 + " is to " + right1, emojis)
            await message.channel.send(response)

        else:
            await message.channel.send("Usage: To answer a is to b as c is to _, type '.anal a b c'")

    if message.content.startswith(".ass") or message.content.startswith(".association"):
        message_words = message.content.split()[1:]
        if len(message_words) == 1:
            chosen_word = message_words[0]
            response = print_most_similar(words, chosen_word)
            if response == "Unknown word.":
                response = print_most_similar(words, chosen_word.capitalize())
            if response == "Unknown word.":
                response = print_most_similar(words, spell(chosen_word.lower()))
            if response == "Unknown word.":
                response = print_most_similar(words, unemojify(chosen_word))
            emojis = list(client.emojis)
            response = process_emojis2(response, emojis, splitter=", ")
            await message.channel.send(response)
        else:
            await message.channel.send("Usage: To find word associations with 'word', type '.ass word'")
    if message.content.startswith(".impersonate"):
        response, should_train_model, model_folder, train_folder, hook = build_impersonation_data(message)
        await message.channel.send(response)
        if should_train_model:
            delete_obsolete_folder(model_folder)
            delete_obsolete_folder(train_folder)
            setup_params(model_folder, train_folder, hook)
            train_impersonation_model(model_folder)
            restore_params()
    if False: #message.content.startswith(".e2p"):
        message_words = message.content.split()[1:]
        if len(message_words) == 1:
            await e2p(message_words[0], lambda f: client.send_file(message.channel, 'image.png'))
        else:
            await message.channel.send("Please supply one URL to be pornified")
    if message.content.startswith(".nonce"):
        content = message.content[len(".nonce"):]
        raw_msgs = ""
        for i in range(15):
            raw_msgs += await respond(content, model)
        spellchecker.set_text(raw_msgs)
        msg_list = [err.word for err in spellchecker if err.word not in corpus]
        msg = " ".join(msg_list)[:1993]
        await message.channel.send(msg)


async def respond(message_content, model):
    emojis = list(client.emojis)
    msg = predict_lstm.sample(model, randint(50, 1000), prime=message_content, top_k=5)
    #msg = ' :zizek_4: :usguy: :zizek_4: :zizek_4:  '
    msg = process_emojis2(msg, emojis)
    '''last_answer = message_content
    for i in range(0, 3):
        best_answer, answers = learn_answer(last_answer)
        inf.print_answers(answers)
        msg += best_answer + " "
        last_answer = best_answer
        await asyncio.sleep(0.05)'''
    '''
    msg_list = msg.split()
    msg = ""
    for i, word in enumerate(msg_list):
        word = process_emojis(word, emojis)
        if i != 0:
            if msg_list[i-1] != word:
                msg += word + " "
        else:
            # Higher chance to place an emoji at the start of the message
            if random() < 0.2:
                msg += get_random_emoji(emojis)
            msg += word + " "
        if random() < 0.02:
            msg += get_random_emoji(emojis)
    '''
    # TODO undo need by changing training data
    msg = re.sub(r'\{Attachments\}', '', msg)
    msg = re.sub(r'\{Embed\}', '', msg)
    msg = re.sub(r'\{Reactions\}', '', msg)

    # Trim consecutive newlines
    #msg = re.sub(r'\n\s*\n', '\n\n', msg)
    msg = re.sub(r'\n\s*\n', '\n', msg)

    # Higher chance to place an emoji at the end of the message
    if random() < 0.2:
        msg += " " + get_random_emoji(emojis)
    print(msg[:1993])
    return msg[:1993]

def find_spellchecked_analogy(message_words):
    spellchecked_message_words = [spell(w) for w in message_words]
    print(spellchecked_message_words)
    left2, left1, right2, right1 = print_analogy(*spellchecked_message_words, words)
    return right1

def find_capitalized_analogy(message_words):
    for i, m_word in enumerate(message_words):
        cap_m_word = m_word.capitalize()
        lower_m_word = m_word.lower()
        cap_word_matches = [True for w in words if w.text == cap_m_word]
        lower_word_matches = [True for w in words if w.text == lower_m_word]
        print(cap_word_matches, lower_word_matches)
        if len(cap_word_matches) > len(lower_word_matches):
            message_words[i] = cap_m_word
        elif len(lower_word_matches) > 0:
            message_words[i] = lower_m_word
    print(message_words)
    left2, left1, right2, right1 = print_analogy(*message_words, words)
    return right1


def unemojify(word):
    if word.startswith('<') and word.endswith(">"):
        split_colon = word.split(':')
        if len(split_colon) > 2:
            return ':' + split_colon[1] + ':'
    return word


def process_emojis(word, emojis):
    if word.startswith(":") and word.endswith(":"):
        for emoji in emojis:
            if emoji.name == word[1:-1]:
                return str(emoji)
    return word


def process_emojis2(message, emojis, splitter="whitespace"):
    if splitter != "whitespace":
        message_corpus = message.split(splitter)
    else:
        message_corpus = message.split()
    for word in set(message_corpus):
        if word.startswith(":") and word.endswith(":"):
            for emoji in emojis:
                if emoji.name == word[1:-1]:
                    message = message.replace(word, str(emoji))
                    break
    return message

def get_random_emoji(emojis):
    return str(choice(emojis)) + " "

def build_impersonation_data(message):
    print(message.content)
    found_user = False
    should_train_model = False
    impersonated = ""
    model_folder = ""
    train_folder = ""
    from_file = ""
    to_file = ""
    hook = ""
    for word in message.content.split():
        for member in client.get_all_members():
            if member.id in word:
                impersonated = member.name + "#" + member.discriminator
                hook = "." + member.name.lower().replace(" ", "")[:20]
                found_user = True
    if found_user:
        data_size, model_folder, train_folder = build_training_data(impersonated)
        if data_size < 100:
            response = "Found user " + impersonated + " but there were only" + str(data_size) + " line(s) of messages. Not enough to create a model.",
        else:
            response = "Found user " + impersonated + ". Found " + str(data_size) + " lines of messages to create a model. Training the model may take a long time. **Once training is finished,** type '" + hook + " sup' to talk to the new model."
            should_train_model = True
    else:
        response = "User not found or malformed request. Usage: '.impersonate @User' to build a model based on that person. Training the model may take a long time. Once the model is finished, type '.user sup' to talk to the new model. For example, to talk to an Edza#0255 bot, use '.edza sup'."
    return response, should_train_model, model_folder, train_folder, hook

def delete_obsolete_folder(folder):
    if os.path.isdir(folder):
        try:
            shutil.rmtree(folder)
        except Exception as e:
            print("Could not remove folder " + folder + ", exception: " + str(e))
    else:
        print("Folder " + folder + " does not exist, so nothing to remove.")

@client.event
async def on_ready():
    print('Logged in as')
    print(client.user.name)
    print(client.user.id)
    print('------')

words = load_words('nlp/data/combined.vec')

while True:
    try:
        with open('token.txt', 'r') as tokenfile:
            client.run(tokenfile.read().rstrip())
    except Exception as e:
        # Print the full stack trace of the exception, rather than crashing the program.
        print(traceback.format_exc())
        # Reset the event loop
        loop = asyncio.get_event_loop()
        if not loop.is_closed():
            loop.close()
        asyncio.set_event_loop(asyncio.new_event_loop())
        # Sleep for a short time before starting so we don't spam connection attempts
        time.sleep(5)
