# Importing libraries
import numpy as np
import torch
from torch import nn
import argparse
import joblib
from collections import defaultdict
from functools import partial

# Check if GPU is available
train_on_gpu = torch.cuda.is_available()
device = torch.device("cuda") if train_on_gpu else torch.device("cpu")

# Defining method to encode one hot labels
def one_hot_encode(arr, n_labels):

    # Initialize the the encoded array
    one_hot = np.zeros((np.multiply(*arr.shape), n_labels), dtype=np.float32)

    # Fill the appropriate elements with ones
    one_hot[np.arange(one_hot.shape[0]), arr.flatten()] = 1.

    # Finally reshape it to get back to the original array
    one_hot = one_hot.reshape((*arr.shape, n_labels))

    return one_hot

# Defining method to make mini-batches for training
def get_batches(arr, batch_size, seq_length):
    '''Create a generator that returns batches of size
       batch_size x seq_length from arr.

       Arguments
       ---------
       arr: Array you want to make batches from
       batch_size: Batch size, the number of sequences per batch
       seq_length: Number of encoded chars in a sequence
    '''

    batch_size_total = batch_size * seq_length
    # total number of batches we can make
    n_batches = len(arr)//batch_size_total

    # Keep only enough characters to make full batches
    arr = arr[:n_batches * batch_size_total]
    # Reshape into batch_size rows
    arr = arr.reshape((batch_size, -1))

    # iterate through the array, one sequence at a time
    for n in range(0, arr.shape[1], seq_length):
        # The features
        x = arr[:, n:n+seq_length]
        # The targets, shifted by one
        y = np.zeros_like(x)
        try:
            y[:, :-1], y[:, -1] = x[:, 1:], arr[:, n+seq_length]
        except IndexError:
            y[:, :-1], y[:, -1] = x[:, 1:], arr[:, 0]
        yield x, y


def return_default_char():
    return " "


default_num = 0


def return_default_num():
    return default_num


# Declaring the model
class CharRNN(nn.Module):

    def __init__(self, tokens, n_hidden=1024, n_fc=205, n_layers=2,
                               drop_prob=0.64, lr=0.001, token_dict=None):
        super().__init__()
        self.drop_prob = drop_prob
        self.n_layers = n_layers
        self.n_hidden = n_hidden
        self.n_fc = n_fc
        self.lr = lr

        # creating character dictionaries
        self.chars = tokens
        if token_dict:
            self.int2char = token_dict['int2char']
            self.char2int = token_dict['char2int']
        else:
            d_int = dict(enumerate(self.chars))
            d_char = {ch: ii for ii, ch in d_int.items()}
            global default_num
            if " " in d_char:
                default_num = d_char[" "]
            else:
                default_num = 0

            self.int2char = defaultdict(return_default_char, d_int)
            self.char2int = defaultdict(return_default_num, d_char)

        # define the LSTM
        self.lstm = nn.LSTM(self.n_fc, n_hidden, n_layers,
                            dropout=drop_prob, batch_first=True)

        # define a dropout layer
        self.dropout = nn.Dropout(drop_prob)

        # define the final, fully-connected output layer
        self.fc = nn.Linear(n_hidden, self.n_fc)


    def forward(self, x, hidden):
        ''' Forward pass through the network.
            These inputs are x, and the hidden/cell state `hidden`. '''

        #get the outputs and the new hidden state from the lstm
        r_output, hidden = self.lstm(x, hidden)

        #pass through a dropout layer
        out = self.dropout(r_output)

        # Stack up LSTM outputs using view
        out = out.contiguous().view(-1, self.n_hidden)

        #put x through the fully-connected layer
        out = self.fc(out)

        # return the final output and the hidden state
        return out, hidden


    def init_hidden(self, batch_size):
        ''' Initializes hidden state '''
        # Create two new tensors with sizes n_layers x batch_size x n_hidden,
        # initialized to zero, for hidden state and cell state of LSTM
        weight = next(self.parameters()).data

        if train_on_gpu:
            hidden = (weight.new(self.n_layers, batch_size, self.n_hidden).zero_().cuda(),
                  weight.new(self.n_layers, batch_size, self.n_hidden).zero_().cuda())
        else:
            hidden = (weight.new(self.n_layers, batch_size, self.n_hidden).zero_(),
                      weight.new(self.n_layers, batch_size, self.n_hidden).zero_())

        return hidden

# Declaring the train method
def train(net, data, epochs=100, batch_size=4, seq_length=50, lr=0.001, clip=5, val_frac=0.1, print_every=10):
    ''' Training a network

        Arguments
        ---------

        net: CharRNN network
        data: text data to train the network
        epochs: Number of epochs to train
        batch_size: Number of mini-sequences per mini-batch, aka batch size
        seq_length: Number of character steps per mini-batch
        lr: learning rate
        clip: gradient clipping
        val_frac: Fraction of data to hold out for validation
        print_every: Number of steps for printing training and validation loss

    '''
    net.train()

    opt = torch.optim.Adam(net.parameters(), lr=lr)
    criterion = nn.CrossEntropyLoss()
    #scheduler = torch.optim.lr_scheduler.StepLR(opt, 25, gamma=0.5)


    # create training and validation data
    val_idx = int(len(data)*(1-val_frac))
    data, val_data = data[:val_idx], data[val_idx:]

    if(train_on_gpu):
        net.cuda()

    counter = 0
    n_chars = net.n_fc
    for e in range(epochs):
        # initialize hidden state
        h = net.init_hidden(batch_size)

        for x, y in get_batches(data, batch_size, seq_length):
            counter += 1

            # One-hot encode our data and make them Torch tensors
            x = one_hot_encode(x, n_chars)
            inputs, targets = torch.from_numpy(x), torch.from_numpy(y)

            if(train_on_gpu):
                inputs, targets = inputs.cuda(), targets.cuda()

            # Creating new variables for the hidden state, otherwise
            # we'd backprop through the entire training history
            h = tuple([each.data for each in h])

            # zero accumulated gradients
            net.zero_grad()

            # get the output from the model
            output, h = net(inputs, h)

            # calculate the loss and perform backprop
            loss = criterion(output, targets.view(batch_size*seq_length).long())
            loss.backward()
            # `clip_grad_norm` helps prevent the exploding gradient problem in RNNs / LSTMs.
            nn.utils.clip_grad_norm_(net.parameters(), clip)
            opt.step()

            # loss stats
            if counter % print_every == 0:
                # Get validation loss
                val_h = net.init_hidden(batch_size)
                val_losses = []
                net.eval()
                for x, y in get_batches(val_data, batch_size, seq_length):
                    # One-hot encode our data and make them Torch tensors
                    x = one_hot_encode(x, n_chars)
                    x, y = torch.from_numpy(x), torch.from_numpy(y)

                    # Creating new variables for the hidden state, otherwise
                    # we'd backprop through the entire training history
                    val_h = tuple([each.data for each in val_h])

                    inputs, targets = x, y
                    if(train_on_gpu):
                        inputs, targets = inputs.cuda(), targets.cuda()

                    output, val_h = net(inputs, val_h)
                    val_loss = criterion(output, targets.view(batch_size*seq_length).long())

                    val_losses.append(val_loss.item())

                net.train() # reset to train mode after iterationg through validation data

                print("Epoch: {}/{}...".format(e+1, epochs),
                      "Step: {}...".format(counter),
                      "Loss: {:.4f}...".format(loss.item()),
                      "Val Loss: {:.4f}".format(np.mean(val_losses)))
        #scheduler.step()

def main():
    # Parse command line arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('--transfer', '-t', help='Transfer learn from specified torch file.', type=str)
    args = parser.parse_args()

    # Open text file and read in data as `text`
    with open('new_data/train.to', 'r') as f:
        text = f.read().encode()

    # Showing the first 100 characters
    print(text[:100])

    # encoding the text
    chars = tuple(set(text))

    if train_on_gpu:
        print('Training on GPU!')
    else:
        print('No GPU available, training on CPU; consider making n_epochs very small.')
    # Define and print the net
    n_hidden=1024
    n_layers=2
    if args.transfer:
        pretrained = torch.load(args.transfer, map_location=device)
        net = CharRNN(chars, pretrained['n_hidden'], pretrained['n_fc'], pretrained['n_layers'], token_dict=pretrained)
        net.load_state_dict(pretrained['state_dict'])
    else:
        net = CharRNN(chars, n_hidden, len(chars), n_layers)
    print(net)

    # Declaring the hyperparameters
    batch_size = 256
    seq_length = 300
    n_epochs = 30 # start smaller if you are just testing initial behavior

    # Encode the text
    encoded = np.array([net.char2int[ch] for ch in text])
    # train the model
    train(net, encoded, epochs=n_epochs, batch_size=batch_size, seq_length=seq_length, lr=0.001, print_every=50)

    # Saving the model
    model_name = 'rnn_20_epoch.net'

    checkpoint = {
        'n_hidden': net.n_hidden,
        'n_fc': net.n_fc,
        'n_layers': net.n_layers,
        'state_dict': net.state_dict(),
        'tokens': net.chars,
        'int2char': net.int2char,
        'char2int': net.char2int
    }

    with open(model_name, 'wb') as f:
        torch.save(checkpoint, f)

if __name__ == '__main__':
    main()
