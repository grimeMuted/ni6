import torch
from collections import defaultdict
import argparse
from lstm import CharRNN, return_default_num, return_default_char

parser = argparse.ArgumentParser()
parser.add_argument('--transfer', '-t', help='Transfer learn from specified torch file.', type=str)
args = parser.parse_args()

# Open text file and read in data as `text`
with open('new_data/train.to', 'r') as f:
    text = f.read().encode()

# Showing the first 100 characters
print(text[:100])

# encoding the text
chars = tuple(set(text))

train_on_gpu = torch.cuda.is_available()
device = torch.device("cuda") if train_on_gpu else torch.device("cpu")
if args.transfer:
    pretrained = torch.load(args.transfer, map_location=device)

    net = CharRNN(chars, pretrained['n_hidden'], 205, pretrained['n_layers'], token_dict=pretrained)
    net.load_state_dict(pretrained['state_dict'])
else:
    exit(0)

d_int = dict(enumerate(chars))
d_char = {ch: ii for ii, ch in d_int.items()}
if " " in d_char:
    default_num = d_char[" "]
else:
    default_num = 0

# Saving the model
model_name = 'testit.net'

print(len(net.char2int.items()))

checkpoint = {
    'n_hidden': net.n_hidden,
    'n_fc': net.n_fc,
    'n_layers': net.n_layers,
    'state_dict': net.state_dict(),
    'tokens': net.chars,
    'int2char': net.int2char,
    'char2int': net.char2int,
}

with open(model_name, 'wb') as f:
    torch.save(checkpoint, f)

print(torch.load(model_name)['char2int'])
