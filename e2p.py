import aiohttp
import asyncio
import json

async def fetch(session, api_url, image_url):
    params = {
       "images": [image_url],
    }
    async with session.post(api_url, data=json.dumps(params)) as response:
        return await response.read()

async def e2p(image_url, callback):
    headers = {
        "Authorization": "Bearer 9ce62a526b4aab7de4d1af9e95aeed",
        "Content-Type": "application/json"
    }
    async with aiohttp.ClientSession(headers=headers) as session:
        res = await fetch(
            session,
            #'https://fapi.wrmsr.io/blackify',
            'https://fapi.wrmsr.io/edges2porn',
            image_url
        )
        print(res)
        with open('image.png', 'wb') as image_file:
            image_file.write(res)
        with open('image.png', 'rb') as image_file:
            await callback(image_file)

