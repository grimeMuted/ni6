import sys, re

def insert_newlines(book_file):
    with open(book_file) as book, open(book_file + ".nl", 'w') as dest:
        processed_data = ""
        data = book.read().split()
        for i, word in enumerate(data):
            processed_data += word + " "
            if i % 8 == 7:
                processed_data += "\n"
        dest.write(processed_data)

if len(sys.argv) > 1:
    insert_newlines(sys.argv[1])
