import sys, json, re

FROM_FILE = "train.from"
TO_FILE = "train.to"

def split_discord(discord_file):
    with open(discord_file) as discord, open(FROM_FILE, 'a+') as from_file, open(TO_FILE, 'a+') as to_file:
        regex = re.compile('[^a-zA-Z\.\,\;\:\'\?\!]')
        text = json.load(discord)
        i = 0
        data = text["data"]
        #messages = data["data"]["419584341333770252"]
        for _, messages in data.items():
            for k in sorted(messages):
                v = messages[k]
                if "m" in v:
                    print(v["m"])
                    raw_message = ""
                    for word in v["m"].split():
                        if not "http" in word:
                            if not (len(word) == 1 and word.lower() != "i" and word.lower() != "u"):
                                raw_message += word + " "
                    message = regex.sub(' ', raw_message) + '\n'
                    if i % 2:
                        from_file.write(message)
                    else:
                        to_file.write(message)
                    i += 1


if len(sys.argv) > 1:
    split_discord(sys.argv[1])
