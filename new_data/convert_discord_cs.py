import sys, json, re

DATA_FILE = "train.txt"



def split_discord(discord_file):
    with open(discord_file) as discord, open(DATA_FILE, 'a+') as data_file:
        for line in discord.readlines():
            if (line.startswith("[") and line[-2].isdigit()) or len(line) < 3:
                continue
            raw_message = ""
            for word in line.split():
                if "http" in word:
                    continue
                if word.startswith("@") and word[-1].isdigit():
                    continue
                if not (len(word) == 1 and word.lower() != "i" and word.lower() != "u"):
                    raw_message += word + " "
            message = raw_message + '\n'
            data_file.write(message)


if len(sys.argv) > 1:
    split_discord(sys.argv[1])
