import sys

def combine_short_lines(book_file):
    with open(book_file) as book, open(book_file + ".nl", 'w') as dest:
        processed_data = ""
        data = book.readlines()
        for line in data:
            processed_data += line.rstrip()
            if len(processed_data.split()) >= 5:
                dest.write(processed_data + '\n')
                processed_data = ""
            else:
                processed_data += " "

if __name__ == '__main__' and len(sys.argv) > 1:
    combine_short_lines(sys.argv[1])
