import sys, re

DATA_FILE = "train.txt"

def split_book(book_file):
    alpha_regex = re.compile('[^a-zA-Z \n0-9]')
    upper_regex = re.compile('[A-Z]')
    with open(book_file) as book, open(DATA_FILE, 'a+') as data_file:
        for i, line in enumerate(book):
            line = alpha_regex.sub('', line)
            if " p " in line or len(line) < 3 or "<" in line or ">" in line:
                continue
            data_file.write(line)


if len(sys.argv) > 1:
    split_book(sys.argv[1])
