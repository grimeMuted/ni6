import sys, json, re

FROM_FILE = "train.from"
TO_FILE = "train.to"



def split_discord(discord_file):
    with open(discord_file) as discord, open(FROM_FILE, 'a+') as from_file, open(TO_FILE, 'a+') as to_file:
        i = 0
        for line in discord.readlines():
            if (line.startswith("[") and line[-2].isdigit()) or len(line) < 3:
                continue
            raw_message = ""
            for word in line.split():
                if "http" in word:
                    continue
                if word.startswith("@") and word[-1].isdigit():
                    continue
                if not (len(word) == 1 and word.lower() != "i" and word.lower() != "u"):
                    raw_message += word + " "
            message = raw_message + '\n'
            if i % 2:
                from_file.write(message)
            else:
                to_file.write(message)
            i += 1


if len(sys.argv) > 1:
    split_discord(sys.argv[1])
