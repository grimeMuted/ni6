import sys, re

FROM_FILE = "train.from"
TO_FILE = "train.to"

def split_book(book_file):
    alpha_regex = re.compile('[^a-zA-Z \n0-9]')
    upper_regex = re.compile('[A-Z]')
    with open(book_file) as book, open(FROM_FILE, 'a+') as from_file, open(TO_FILE, 'a+') as to_file:
        for i, line in enumerate(book):
            line = alpha_regex.sub('', line)
            if " p " in line or len(line) < 3 or "<" in line or ">" in line:
                continue
            #for word in line.split():
            #    if word == upper_regex.
            if i % 2:
                from_file.write(line)
            else:
                to_file.write(line)


if len(sys.argv) > 1:
    split_book(sys.argv[1])
