import sys, json, re

FROM_FILE = "train.from"
TO_FILE = "train.to"

BANNED_LINES = [
    "Twitter",
    "Retweets",
    "Likes",
    "Pinned message."
]

def split_discord(discord_file):
    with open(discord_file) as discord, open(FROM_FILE, 'a+') as from_file, open(TO_FILE, 'a+') as to_file:
        i = -1
        for line in discord.readlines():
            skip_line = False
            for banned in BANNED_LINES:
                if banned in line:
                    skip_line = True
            if line.isspace():
                skip_line = True
            if not skip_line:
                i += 1
                if i % 2:
                    from_file.write(line)
                else:
                    to_file.write(line)


if len(sys.argv) > 1:
    split_discord(sys.argv[1])
