import sys, json, re

DATA_FILE = "train.txt"

def split_discord(discord_file):
    with open(discord_file) as discord, open(DATA_FILE, 'a+') as data_file:
        regex = re.compile('[^a-zA-Z\.\,\;\:\'\?\!]')
        text = json.load(discord)
        data = text["data"]
        for _, messages in data.items():
            for k in sorted(messages):
                v = messages[k]
                if "m" in v:
                    print(v["m"])
                    raw_message = ""
                    for word in v["m"].split():
                        if not "http" in word:
                            if not (len(word) == 1 and word.lower() != "i" and word.lower() != "u"):
                                raw_message += word + " "
                    message = regex.sub(' ', raw_message) + '\n'
                    data_file.write(message)

if len(sys.argv) > 1:
    split_discord(sys.argv[1])
