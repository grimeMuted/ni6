THE UPANISHADSBY SWAMI PARAMANANDA My
Portrait of the author. Flexible
Creed. (Poems.) 2.00. Cloth, $1.50.
Postage 10 cts. binding, of
Flexible Rhythm of Life. (Third Volume
Poems.) Cloth, $1.50. Postage 10 cts.
binding, $2.00. The Vigil. (Second Volume of Poems.) Portrait of the
author. Flexible binding, $2.00. Cloth, $1.50.
Postage 10 cts. Poems. Flexible
Soul'* Secret Door. (Second Edition.)
Cloth, ink lettering, binding, gold lettering, $2.00.
$1.50. Postage 10 cts. Book of Daily Thoughts and Prayers. Flexible bindings,
Cloth, $2.00. Leather, $3.50. $2.75 and $2.50.
Postage 15 cts. The Path of Devotion. I Sixth Edition.) Cloth, $1.00.
De luxe 51.50. Postage 10 cts
The Way of Peace and Blessedness 3rd Edition.) Por-
trait of author. Cloth, $1.00. Paper, 85 cts., Postage
10 cts. Vedanta in Practice. (Third Edition.)
Cloth, $1.00. Paper, 85 cts. Postage 10 cts.
Reincarnation and Immortality. Postage Cloth, $1.00.
10 cts. Flexible binding, 50 cts.
Postage Right Resolutions. extra. Healing Meditations.
Flexible binding, 50 cts. Postage
extra. PRACTICAL SERIES Cloth. Full set $4.25. Postage extra.
Concentration* and Meditation. New Edition, $1.00.
Faith as a Constructive Force.
*| New Edition. Self-Mastery. 75c each
> Creative Power of Silence.
Spiritual Healing. Secret of Right Activity.
COMPARATIVE STUDY SERIES Emerson and Vedanta. Cloth $1.00. Post, extra.
Post, extra. Cloth $1.00 Christ and Oriental Ideal*.
Post, extra. Plato and Vedic Idealism. Cloth $1.00.
TRANSLATIONS FROM THE SANSKRIT (Third Edition.) Vol. 1. With lucid
#2.00 Postage 8 cts. (Fourth Edition.) Flexible cloth, gold
Bhagavad-Gita. Yellow cloth, ink lettering, $1.00.
lettering, $1.50. Postage 6 cents.
The Upanishadt. commentary. THE VEDANTA CENTRE,
420 Beacon, Boston, Mass., U. S. A.
ANANDA-ASHRAMA La Crescenta, Los Angeles County, California, U.S.A.THE UPANISHADS
TRANSLATED AND COMMENTATED BY SWAMI PARAMANANDA
FROM THE ORIGINAL SANSKRIT TEXT
VOLUME I THIRD EDITION Enlarged
PUBLISHED BY THE VEDANTA CENTRE
BOSTON MASS U S ACOPYRIGHT, 1919, BY
SWAMI PARAMANANDA THE-PLIMPTON-PKESS N OHWOOD-MASS-U-S-AAnnex
5015857 VOLUME IS REVERENTLY DEDICATED TO ALL SEEKERS
OF TRUTH AND LOVERS OF WISDOMPREFACE
A HE translator's idea of rendering the
Upanishads into clear simple English, ac-
cessible to Occidental readers, had
its origin Boston friend in 1909.
The gentleman, then battling with a fatal
malady, took from his library shelf a trans-
in a visit paid to a
the Upanishads and, opening it,
expressed deep regret that the obscure and
unfamiliar form shut from him what he
lation of felt to be profound and vital teaching.
desire to unlock the closed doors of
The this ancient treasure house,
awakened at that time, led to a series of classes on the
Upanishads at The Vedanta Centre of
Boston during Street. The its early days in St. Botolph
translation and commentary given were
studious revision, then transcribed and,
were published in after the
Centre's monthly magazine, "The Message
of the East," in 1913 and 1914. Still
further revision has brought form.
it to its present8 Preface
So was consistent with a
far as the Sanskrit text, the
rendering of his throughout eliminate
has translation faithful Swami to
sought that might seem obscure and
While to the modern mind.
all confusing retaining in remarkable measure the rhythm
and archaic force of the lines, he has tried
not to sacrifice directness and simplicity of
Where he has been obliged to use
style. the term Sanskrit for
lack of an exact English equivalent, he has invariably inter-
it by a familiar English word in
preted brackets; and everything has been done to
remove the sense of strangeness in order that
the Occidental reader an alien in the
new may not feel himself
regions of thought opened to him.
Even more has the Swami
striven to keep the letter subordinate to the
Scripture document. is To treat it
intellectual curiosity is of its
spirit. Any only secondarily an historical
deeper message. as an object of mere
to cheat the world If
mankind is to derive the highest benefit from a study of
its appeal must be primarily to
the it, spiritual consciousness; and
one of the salient merits of the present translation lies9
Preface in this, that the translator approaches his
task not only with the grave concern of the
careful scholar, but also with the profound
reverence and fervor of the true devotee.
EDITOR BOSTON, March, 1919CONTENTS PAGE
INTRODUCTION 13 ISA-UPANISHAD 25 KATHA-UPANISHAD 39
KENA-UPANISHAD 95 MUNDAKA-UPANISHAD 121INTRODUCTION HE
Upanishads represent the loftiest Indo-Aryan thought and
culture. They form the wisdom portion
JL heights of ancient Gndna-Kdnda
or trasted ficial with of
the Vedas, Karma-Kdnda the con-
as or sacri- In each of the four great
portion. Vedas known as Rik, Yajur, Sama and
there is a large portion which
Atharva deals predominantly with rituals and cere-
monials, and which has for its aim to show
man how by the path of right action he
may prepare himself for higher attainment. Fol-
lowing this in each Veda is another portion
the Upanishad, which deals wholly
with the essentials of philosophic discrimi-
nation and ultimate spiritual vision. For
called this reason the Upanishads are known as
is, the end or final goal
the Veddnta, that of wisdom (Veda, wisdom; anta, end).
The name Upanishad has been variously
interpreted. pound Many Sanskrit claim that
word it is Upa-ni-shad, a com-
signi-Introduction 14 " fying sitting at the feet or in the presence
of a teacher"; while according to other
authorities it means "to shatter" or "to
destroy" the fetters of ignorance. What-
ever may have been the technical reason
for selecting this doubtedly to
name, give a it was chosen un-
picture of aspiring "approaching" some wise Seer in
the seclusion of an Himalayan forest, in
order to learn of him the profoundest
truths regarding the cosmic universe and
seekers God. Because these teachings were usually
given in the stillness of
where the noises of some distant
the retreat, world could not
disturb the tranquillity of the contemplative
life, they are known also as Aranyakas,
Another reason for this name may be found in the fact that they
were intended especially for the Vdna-
Forest Books. prasthas (those who, having fulfilled all
their duties in the world, had retired to the
forest to devote themselves to
spiritual study). The form which the teaching naturally
assumed was that of dialogue, a form later
adopted by Plato and other Greek philoso-
As nothing was written and all
phers.Introduction instruction was Upanishads are
sense transmitted called of orally,
Srutis, The term was heard."
1 also "what 5 the
is used in the the
Upanishads being regarded as direct revelations of God;
while the Smritis, minor Scriptures "re-
revealed, corded through memory," were traditional
works of purely human origin. It is a
significant fact that ishads is
nowhere mention made of in the
Upan- any author or recorder.
No date for the origin of the Upanishads
fixed, because the written text does
can be not limit their antiquity.
makes that bears to us.
The word Sruti The teaching
ages before it was set
written form. The text itself
any evidence of this, because not in-
probably down clear existed in
frequently in a dialogue between teacher
and disciple the teacher quotes from earlier
As Pro- Scriptures now unknown to us.
fessor Max Miiller states in his lectures
on the Vedanta Philosophy: "One feels certain
that behind all these lightning-flashes of
and philosophic thought there is
a distant past, a dark background of which
we shall never know the beginning." Some
religious1 6 Introduction scholars place the Vedic period as far back
as 4000 or 5000 B.C.; others from 2000 to
But even the most conservative
1400 B.C. admit that it
by antedates, cen- several which
turies at least, the Buddhistic period
begins in the sixth century B.C.
The value of the Upanishads, however,
does not rest upon their antiquity, but upon
the vital message they contain for
and all liarly There peoples.
or local racial all times
is in nothing pecu- them. The en-
nobling lessons of these Scriptures are as
practical for the modern world as they
were for the Indo-Aryans of the earliest
Vedic age. Their teachings are summed
up in M aha-Vdkyam two
or "great say- twam asi (That thou art)
and Aham Brahmdsmi (I am Brahman).
This oneness of Soul and God lies at the
Tat ings": very root of
all Vedic .thought, and it
is this dominant ideal of the unity of all life
and the oneness of Truth which makes the
study of the Upanishads especially bene-
at the present One of the most
ficial Orientalists writes: upon it
(this moment. eminent "If we
fix of European our attention
fundamental dogma of theIntroduction 1
7 Vedanta system) in its philosophical sim-
as the plicity identity of God and the Soul,
the Brahman and the Atman, it will be
found far possess a significance reaching
beyond the Upanishads, their time and
to country; nay, we claim for it an inestimable
value for the whole race of mankind.
. . . Whatever new and unwonted paths the
philosophy of the future may strike out,
this principle will remain permanently un-
shaken and from it no deviation can pos-
sibly take place. If ever a general solution
reached of the great riddle
the key can only be found where alone the secret
is . . . open to us from within, that
to say, in our innermost self.
It was of nature lies
is here that thinkers for
of the first time the
Upanishads, mortal honor, found it.
." . The the to
original their im- . introduction of the Upanishads
to the Western world was through a trans-
first made in the .seventeenth
More than a century later the dis-
French scholar, Anquetil Du- lation into Persian
century. tinguished perron, brought a copy of the manuscript
from Persia to France and translated it into
French and Latin, publishing only
the1 Introduction 8 Latin Despite the distortions which
text. must have from resulted
transmission through two alien languages, the light of
the thought still shone with such brightness
drew from Schopenhauer the fervent
words: "How entirely does the Oupnekhat
that it (Upanishad) spirit the
of breathe throughout the holy
Vedas! How is every one,
who by a diligent study of its Persian
has become familiar with that in-
Latin book, stirred comparable by
to the very depth of his Soul!
that spirit From every sentence deep, original and sublime thoughts
arise, and the whole is pervaded by a high
and holy and earnest spirit."
Again he says: "The access to (the Vedas) by means
of the Upanishads is in
eyes the greatest my privilege
which may (1818) turies." this
still claim before This testimony
young all is century previous cen-
borne out by the thoughtful American scholar, Thoreau,
who writes: "What extracts from the
Vedas of a have read fall on me like the light
higher and purer luminary which
I describes a loftier course through a purer
stratum universal." free from particulars,
simple,Introduction The first 19 English translation was
a learned Hindu, Raja made by
Ram Mohun Roy Since that time there have
(1775-1833). various translations European been
French, German, Italian and English. But
.a mere translation, however accurate and
sympathetic, is make not sufficient to
accessible to the the Occidental
Upanishads mind. Professor Max Miiller after a life-
time of arduous labor in this field frankly
confesses: "Modern words are round, an-
words are square, and we may as
well hope to solve the quadrature of the
circle, as to express adequately the ancient
thought of the Vedas in modern English."
cient Without a commentary to
it is practically understand either the
spirit impossible or the meaning of the Upanishads. They
were never designed as popular Scriptures.
They grew up essentially as text
and books of Self-knowledge, and
God-knowledge books they need interpretation.
Being transmitted orally from teacher to
disciple, the style was necessarily extremely
condensed and in the form of aphorisms.
The language also was often metaphorical
and obscure. Yet if one has the perse-
like all text2O Introduction verance to penetrate beneath these mere
surface difficulties, one is repaid a hundred-
fold; for these ancient Sacred
Books contain the most precious
gems of spiritual thought. Every Upanishad begins with a Peace
Chant (Shanti-patha] to create the proper
atmosphere of purity and serenity. To
study about God the whole nature must
be prepared, so unitedly and with loving
hearts teacher and disciples prayed to the
Supreme Being for His grace and protec-
It is not possible to comprehend the
tion. subtle problems of life unless the thought
is tranquil and the energy concentrated.
Until mind our is withdrawn
from the varied distractions and agitations of worldly
affairs, we cannot enter into the spirit of
No study is of religious study.
avail so long as our inner being is not at-
higher tuned. We towards all living
we must must hold a peaceful attitude
things; and if it is lacking,
strive fervently to cultivate it
through suggestion by chanting or repeating
some holy text. The same lesson is taught
by Jesus the Christ when He says:
thou bring thy gift to the altar and there
"If rememberest that thy brother hath aughtIntroduction
2 1 against thee; leave there thy gift before
the altar and go thy way; first be recon-
ciled to offer thy thy brother, and then come and
gift." Bearing minds, let this lofty ideal of
us try to peace in our
make our hearts free from prejudice, doubt and intolerance, so
that from these sacred writings we may
draw in abundance inspiration, love and
wisdom. PARAMANANDAISA-UPANISHADThis Upanishad derives "God-covered."
Isa-vasya, from the opening words,
The use of Isa (Lord)
a its title more personal name of the Supreme Being than Brahman,
Atman or Self, the names usually found in the Upanishads
constitutes one of Us peculiarities. It forms the clos-
ing chapter of the Yajur-Veda, known as Shukla (White).
Oneness of the Soul and God, and the value of both faith
and works as means themes of this
Upanishads is of ultimate attainment are the leading
The general teaching of the
Upanishad. that works alone, even the highest, can bring
only temporary happiness and must inevitably bind a man,
unless through them he gains knowledge of his real Self.
To help all Upanishads. him acquire
this knowledge is the aim
of this andISA-UPANISHAD PEACE CHANT
That enal); Invisible- Absolute) (the
OM! whole; whole is this
(the visible is phenom- from the Invisible Whole comes forth
the visible whole. Though the visible whole
has come out from that Invisible Whole, yet
the Whole remains unaltered. OM! PEACE! PEACE! PEACE!
'TpHE indefinite term "That" is
used in the Upanishads to designate the Invisible- Absolute, because
or name can fully define It.
A no word finite object, like
or a tree, can be defined; but God,
who is infinite a table
and un- bounded, cannot be expressed by finite language. There-
fore the Rishis or Divine Seers, desirous not to limit the
" " to designate Unlimited, chose the indefinite term That
the Absolute. In the light of true wisdom the phenomenal and the
Absolute are inseparable. and whatever
exists, must All existence exist in It;
is in the Absolute; hence
all manifesta- merely a modification of the One Supreme Whole,
and neither increases nor diminishes It. The Whole there-
tion is fore remains unaltered.The Upanisbads
26 ALL whatsoever this, the uni-
in exists verse, should be covered by the Lord.
Having renounced Real) Do .
TXT'E cover (the unreal), enjoy (the
not covet the wealth of any man.
things with the Lord all
Divine Presence everywhere. by perceiving the
When the conscious- firmly fixed in God, the conception of diversity
naturally drops away; because the One Cosmic Existence
ness is shines through all
things. As we gain the
light of wis- dom, we cease to cling to the unrealities of this world,
and we find all our joy in the realm of Reality.
The word "enjoy" is also interpreted by the great
" commentator Sankaracharya as protect," because knowl-
of our true Self is
the edge greatest protector and sus-
tainer. If we do not have this knowledge, we cannot be
happy; because nothing on this external plane of phenom-
ena is permanent or dependable. He who is rich in the
knowledge of the Self does not covet external
power or possession. II one should desire to
IF a ing Karma mayest
this, not live in hundred years, one should
(righteous live; there is Karma
defile thee. world perform- Thus thou
deeds). no other way. (the fruits of thy
this live By doing actions) willIsa-Upanishad
TF a man and is
27 still clings to long life and earthly possessions,
therefore unable to follow the path of Self-
(Gndna-Nishta) as prescribed in the first
then he may follow the path of right
Karma here means actions per-
action (Karma-Nishld). knowledge Mantram (text),
formed without When a alone.
his lower desires, selfish motive, for the sake of the Lord
man performs actions clinging blindly to
then his actions bind him to the plane of
ignorance or the plane of birth and death; but when the
same actions are performed with surrender to God, they
purify and liberate him. Ill
AFTER have leaving their bodies, they
who killed the Self go to the worlds of
the Asuras, covered with blinding ignorance.
npHE idea of rising to bright regions as a reward for
and of falling into realms of darkness as a
well-doers, punishment for evil-doers is common to all great religions.
But Vedanta claims that this condition of heaven and hell
is only temporary; because our actions, being finite, can
produce only a finite result.
What does it mean "to
kill the Self?" How can the
immortal Soul ever be destroyed? It cannot be destroyed,
Those who hold themselves it can only be obscured.
under the sway of ignorance, who serve the flesh and
A tman or the real Self, are not able to perceive
neglect the the effulgent and indestructible nature of their Soul; hence
they fall into the realm where the Soul light does not
shine. Here the Upanishad shows that the only hell is
As long as man is overpowered
absence of knowledge.The Upanishads 28
by the darkness of ignorance, he is the slave of Nature and
must accept whatever comes as the fruit of his thoughts
and deeds. When he strays into the path of unreality,
the Sages declare that he destroys himself;
who clings to the perishable body and regards
Self must experience death many
because he it as his true
times. IV T VHAT One, though motionless, is swifter
than the mind. The senses can never
overtake It, for It immovable,
run. By ever goes before.
It travels faster Though who
than those It the all-pervading air sustains all
living beings. 'T" V HIS verse explains the character of the Atman or Self.
A finite object can be taken from one place and put
in another, but it can only occupy one space at a time.
The Atman, however, is present
everywhere; hence, run with the greatest swiftness to over-
take It, already It is there before him.
Even the all-pervading air must be supported by this
Self, since It is infinite; and as nothing can live without
breathing air, all living things must draw their life from
though one the Cosmic may
Self. V moves and IT also
is It It is near.
without all this. moves not.
It is within It is far
and and also ItIsa-Upanishad TT
is near to those who have
29 the power to understand
It, every one; but It seems
covered by the clouds of sensual-
for It dwells in the heart of
whose mind is and self-delusion. It is within, because It is the inner-
most Soul of all creatures; and It is without as the essence
of the whole external universe, infilling it like the all-
far to those ity pervading ether.
VI HE who sees all beings in the Self
Self in all beings, from
and the he never turns away
It (the Self). VII HE who
perceives all beings as the Self,
him how can there be delusion or
when he sees this oneness (every-
for grief, where) ? 1LJE who
perceives the Self everywhere never shrinks
from anything, because through his higher conscious-
ness he feels united with all life. When a man sees God
beings in God, and also God dwelling
hate any living thing? Grief
and delusion rest upon a belief in diversity, which leads to
in all beings in his
own and Soul, competition and
all how can he all
forms of selfishness. With the
tion of oneness, the sense of diversity vanishes
cause of misery is removed.
realiza- and theThe Upanishads 30
VIII HE (the Self) is all-encircling,
resplendent, bodiless, spotless, without sinews, pure*
untouched by sin, all He
has disposed things duly for eternal years.
n^HIS text defines the real nature of the Self.
our mind alone can and
all-knowing, all-seeing, transcendent, self-existent; we
When cleansed from the dross of matter, then
behold the vast, radiant, subtle, ever-pure
is spotless Self, the true basis of our existence.
IX enter into blind THEY
worship sion) ness ; they
darkness who Avidya (ignorance and delu-
fall, as it were, into greater dark-
who worship Vidya (knowledge). X
one end is attained; by Avidya,
Thus we have heard from
the wise men who taught this.
BY Vidya another. XI who knows
at the same time both
and Vidya Avidya, crosses over death
and attains by Avidya immortality through
HE Vidya.Isa- Upanishad ' I
3 1 ''HOSE who follow or "worship" the path of selfishness
and pleasure (Avidyd), without knowing anything
higher, necessarily fall into but those
darkness; worship or cherish Vidyd (knowledge)
for mere who intellectual pride and satisfaction, fall into greater darkness, because
the opportunity which they misuse is greater.
In the subsequent verses Vidyd and Avidyd are used in
something the same sense as "faith" and "works" in the
Christian Bible; neither alone can lead to the ultimate
goal, but when taken together they carry one to the Highest.
Work done with unselfish motive purifies the mind and
enables man to perceive his undying nature. From this
he gains inevitably a knowledge of God, because the Soul
and God are one and inseparable; and when he knows
himself to be one with the Supreme and Indestructible
Whole, he realizes his immortality.
XII THEY fall into blind darkness
ship the Unmanifested into greater
manifested. darkness who wor- and they
who worship fall the XIII
the worship of the Unmanifested one
is attained; by the worship of the
BY end manifested, another. Thus we have heard
from the wise men who taught us this.The Upanishads
32 XIV who knows HE
at the same time both
Unmanifested (the cause tion) the
of manifesta- and the destructible or manifested, he
crosses over death through knowledge of the
destructible and attains immortality through
knowledge the of First Cause (Unmani-
fested). IS Upanishad deals chiefly with the
Cause and the visible manifestation; and
particular Invisible the whole trend of its teaching is to show that they are
one and the same, one being the outcome of the other;
hence no perfect knowledge is possible without simultane-
ous comprehension of both. The wise men declare that he
who worships in a one-sided way, whether the visible
or the invisible, does not reach the highest goal.
Only he who has a co-ordinated
understanding of both the visible and the invisible, of matter and spirit, of activity
and that which is behind activity, conquers Nature and
thus overcomes death. By work, by making the mind
steady and by following the prescribed rules given in the
By the light of that
Scriptures, a man gains wisdom.
wisdom he is visible forms.
able to perceive the Invisible Cause in all
Therefore the wise man sees Him in every
manifested form. God They who have a
are never separated from and He
in them. Him. true conception of
They exist in HimIsa- Upanishad
33 XV face of I
disk. O Uncover (Thy of Truth,
Truth is hidden by a golden
Pushan (Effulgent Being)! face) that
I, the worshipper may behold Thee.
XVI O PUSHAN! O Sun, sole traveller of
all, son of the heavens, controller of
Prajapati, withdraw Thy rays and gather up
burning effulgence. Now through Thy
Grace I behold Thy blessed and glorious form.
Thy The Purusha within Thee, I
(Effulgent Being) am who dwells
He. "LJERE the sun, who is the giver of all light, is used as
the symbol of the Infinite, giver of all wisdom. The
seeker after Truth prays to the Effulgent One to control
His dazzling rays, that his eyes, no longer blinded by them,
the Truth. Having perceived It, he proclaims
"Now I see that that Effulgent Being and I are one and
the same, and my delusion is destroyed." By the light of
Truth he is able to discriminate between the real and the
may behold : and the knowledge thus gained convinces him that
one with the Supreme; that there is no difference
between himself and the Supreme Truth; or as Christ said,
unreal, he is "I and
my Father are one."The Upatiishads
34 XVII MAY let O
my life-breath go the to
all- pervading and immortal Prdna, and
body be burned to ashes. Om!
this mind, remember thy deeds!
O member, remember thy deeds!
CEEK not O fleeting results as the
mind, re- Remember! reward of thy actions,
mind! Manlram Strive only for the Imperishable.
This or text is often chanted at the hour of death to
remind one of the perishable nature
eternal nature of the Soul.
distinction When of the body and the
the clear vision of the
between the mortal body and the immortal
Soul dawns in the heart, then all craving for physical
pleasure or material possession drops away; and one can
say, let the body be burned to ashes that the Soul may
attain its freedom; for death is nothing more than the
casting-off of a worn-out garment.
XVIII Lead us to O Lord
Thou knowest all our deeds, remove all evil
and delusion from us. To Thee we offer our
prostrations and supplications again and
(Bright Being)! OAGNI blessedness by the good path.
again. Here ends this Upanishad
!Isa- Upanishad 35 is called Isa-Vasya-Upanishad, that
which gives Brahma-Vidyd or knowledge of the All-
pervading Deity. The dominant thought running through
'TpHIS Upanishad we cannot enjoy
that it is unless we
Lord. If our we universe;
live wisely knowledge and we thus
we have only fragmentary it,
limit ourselves. within his true
He and sees all beings in his Self
beings, he never suffers; because
ish. and perform our duties?
movable or immovable, good or bad, it is
We must not divide our conception of the
for in dividing He who
with the Omnipresent see, "That."
all all are not fully conscious of that which sustains
how can we life, Whatever we
or realize true happiness life
consciously "cover" when he his Self in all
sees all creatures then jealousy, grief and hatred van-
Self, alone can love. That All-pervading One
effulgent, birthless, deathless, pure, untainted
sorrow. of Knowing this, by
is self- sin and he becomes free from the bondage
matter and transcends death. means
realizing the difference identifying oneself
Transcending death between body and Soul and
with the Soul. When we
actually behold the undecaying Soul within us and realize our true
nature, dies we no longer identify ourself with the
and we do not body which
die with the body. Self-knowledge has always been the theme of the Sages;
and the Upanishads deal especially with the knowledge of
the Self and also with the knowledge of God, because there
is no difference between the
and the same. must Self
and God. That which comes out
also be infinite; hence the Self
They are one of the Infinite
is infinite. Whole That isThe Upanishads
36 we are the drops. So long as the drop remains
from the ocean, it is small and weak; but when it
separate is one with the ocean, then it has all the strength of the
the ocean, ocean. Similarly, so long as
man separate from the Whole, he
identifies himself and partakes with
of Its It, is believes himself to be
helpless; but when he then he transcends
omnipotent qualities. all weaknessKATHA-UPANISHADThe Katha-U panishad is probably the most widely
known of all the Upanishads. It was early translated into
Persian and through this rendering first made its way into
Ram Mohun Roy brought out an
has since appeared in various lan-
and English, German and French writers are all
Later Europe. Rdjd English version.
guages; It agreed in pronouncing
of the religion it one of the most perfect expressions
Sir Ed-win of the Vedas.
and philosophy Arnold popularized it by his metrical rendering under the
name of " The Secret of Death," and Ralph Waldo Emerson
gives its story in brief at the close of his essay
on "Immor- tality." There this
is no consensus of opinion regarding the place of
Some authorities Upanishad in Vedic
literature. belong to the Yajur-Veda, others to the Sama-
Veda, while a large number put it down as a part of the
Atharva-Veda. The story is first suggested in the Rig-
declare it Veda; it is
the to told more Katha-U panishad
woven with definitely in the
it the loftiest Yajur-Veda; and in
appears fully elaborated and inter-
Vedic teaching. There is, nothing,
however, to indicate the special place of this final version,
nor has any meaning been found for the name Katha.
The text presents Nachiketas, Hereafter.
and a dialogue between an aspiring disciple,
Ruler of Death regarding the great
theKATHA-UPANISHAD PEACE CHANT He (the Supreme Being) protect us
both, teacher and taught. May He
MAY us. May we acquire
our strength. May study bring us illumina-
tion. May there be no enmity among us.
be pleased with OM! PEACE! PEACE! PEACE!
Part JFit0t i VAjASRAVA, rewards (at
being desirous of heavenly the
Viswajit sacrifice), a gift of all that he possessed. He had
a son by the name of Nachiketas.
made II WHEN the tributed,
offerings faith (the heart of) Nachiketas,
yet reflected: were being (Shraddha)
dis- entered who, though young,The Upanishads
4O III cows have drunk water, eaten
THESE grass and given milk for the
and their senses gives realms.
TN these have last time,
He who lost all vigor.
undoubtedly to goes joyless India 'the idea of sacrifice has always been to give
freely for the joy of giving,
in return; without asking anything
and the whole purpose and merit
of the sacrifice the giver entertains the least thought of name,
fame or individual benefit. The special Viswajit sacrifice
is lost, if which Vajasrava was making required of him to give
away all that he possessed. When, however, the gifts were
brought forward to be offered, his son Nachiketas, although
probably a lad about twelve years of age, observed how
worthless were the animals which his father was offering.
His heart at once became filled with Shraddhd. There is
no one English word which can convey the meaning of this
Sanskrit term. It is more than mere faith. It also
implies self-reliance, an independent sense of right and
wrong, and the courage of one's
of tender age, Nachiketas father's action:
yet, impelled his higher nature,
own conviction. had no by the sudden awakening
he could not but giving these useless cows,
As a boy right to question his
reflect: "By my father cannot gain
of merely any merit. he has vowed to give all his possessions, then he must also
give me. Otherwise his sacrifice will not be complete and
If fruitful." Therefore, anxious for his father's welfare, he
approached him gently and reverently.Katha-U panishad
41 IV Dear said to his father:
HE whom father, to wilt thou give me?
He said it a second time, then a third time. The father
replied: I shall give thee unto Death.
, being a dutiful son and eager to atone
for his father's him thus
inadequate indirectly that he sacrifice, tried to
had not fulfilled his remind
promise to away all his possessions, since he had not yet
own son, who would be a worthier gift than
give offered his useless His father, conscious that he was not making a
true sacrifice, tried to ignore the boy's questions; but
cattle. by his persistence, he at last impatiently made
answer: "I give thee to Yama, the Lord of Death." The
fact that anger could so quickly rise in his heart proved
that he had not the proper attitude of a sacrificer, who
irritated must always be tranquil, uplifted
and free from egoism. V
I^TACHIKETAS thought: Among many -L^l (of
my father's pupils) I stand first;
among many (others) I stand in the middle
(but never last). for my father by
TT What will my going
be accomplished this day to
Yama? was not conceit which led Nachiketas to consider his
standing and importance. He was weighing his
value as a son and pupil in order to be able to judge whether
or not he had merit enough to prove a worthy gift. Al-
though he realized that his father's harsh reply was only
ownThe Upanishads 42 the expression of a momentary outburst of anger; yet he
believed that greater harm might befall his father, if his
word was not kept. Therefore he sought to strengthen
his father's resolution by reminding him
condition of He life. of the transitory
said: VI back LOOK look
who lived before and who live now. Like
to those those to grain the mortal decays
springs A LL up (is
reborn) and like grain again
. things perish, Truth alone remains. Why then
to sacrifice me also? Thus Nachiketas con-
fear vinced his father that he should remain true to his word
and send him to Yama,
the Ruler of Death. Then Nachi-
ketas went to the abode of Death, but Yama was absent
and the boy waited without food or drink for three days.
On Yama's return one of his household said to him:
VII fire LIKE houses. offering.
Brdhmana guest enters into That fire is quenched by an
a (Therefore) O Vaivaswata, bring
water. VIII foolish THE Brdhmana
all man in whose house a
guest remains without food, his hopes and expectations, all the meritKatha-V panishad
43 gained by his association with the holy, by his
good words and deeds, all his sons and cattle,
are destroyed. A CCORDING to the ancient Vedic ideal a guest
is the representative of God and should be received with
due reverence and honor. Especially is this the case with
a Brdhmana or a Sannyasin whose
to God. Any one who
fails life is wholly consecrated
to give proper care to a holy
guest brings misfortune on himself and his household.
When Yama returned, therefore, one of the members of
household anxiously informed him of Nachiketas'
presence and begged him to bring water to wash his feet,
this being always the first service to an arriving guest.
his IX YAMA said: My
guest! O Brahmana! Revered salutations to thee.
As thou hast remained three nights in my house
without food, therefore choose three boons,
O Brahmana. X May Gautama, my
from anxious thought said: NACHIKETAS
father, be free May he lose all anger (towards
and be me) pacified in heart.
May he know and welcome me when I am sent back by
(about me). thee. boons This,
O Death, I choose. is
the first of the threeThe Upanishads
44 XI YAMA replied: Through
my will Aud- dalaki Aruni (thy father) will know
thee, and be again towards thee as before.
He will sleep in peace at night. He will be
free from wrath when he sees thee released
from the mouth of death.
XII lyTACHIKETAS -i-^l heaven there
said: is no In the realm of
fear, thou (Death) art not there; nor is there fear of old age.
Having crossed beyond both hunger and
thirst and being above grief,
(they) rejoice in heaven. XIII
THOU knowest, sacrifice O Death,
the that leads to heaven.
fire- Tell me, who am full of Shraddhd (faith
and yearning). They who live in the realm
this to of I heaven enjoy freedom from death.
beg as my second boon.
ThisKatha-U panishad 45 XIV YAMA
which know I replied: well
that leads to the realm of heaven.
Listen to me. shall tell it to thee.
fire I Know, O Nachiketas, that this is the means of at-
It taining endless worlds and their support.
is hidden in the heart of
all beings. XV then told him that
YAMA the beginning bricks, fire-sacrifice,
of all the worlds; how many and how
what laid for the altar.
Nachiketas repeated all as it was told to him.
Then Death, being pleased with him, again
said: XVI THE great-souled pleased, said to
give thee Yama, being well
him (Nachiketas): I now another boon. This fire
be named after thee. Take
(sacrifice) shall also this garland of
many colors. XVII HE who
performs this Nachiketa fire- three times, being united
sacrifice with the three (mother, father and teacher),The Upanishads
46 and who fulfills the three-fold duty (study of
the Vedas, sacrifice and alms-giving) crosses
over birth and death. Knowing this wor-
shipful shining realizing fire, Him, he
born of Brahman, and attains eternal peace.
XVIII HE who knows fire
sacrifice the three-fold Nachiketa and performs the Nachiketa fire-
with three-fold knowledge, having and being beyond
he rejoices in the realm of heaven.
cast off the fetters of death
grief, XIX thy fire that
which thou hast this is
ONACHIKETAS, leads to heaven, chosen as thy second boon.
call this fire after thy name.
People Ask will the third
boon, Nachiketas. is regarded as "the foundation of all the worlds,"
because it is the revealer of creation. If there were
IT^IRE no fire or light, no manifested form would be visible.
We " read in the Semitic Scriptures, In the beginning the Lord
Therefore, that which stands said, 'Let there be light.'"
in the external universe as one of the purest symbols of the
Divine, also dwells in subtle form in the heart of everyKatha-U panishad
47 living being as the vital energy, the life-force or cause of
existence. Yama now tells Nachiketas how, by performing sacri-
with the three-fold knowledge, he may transcend grief
and death and reach heaven. The three-fold knowledge
fice referred to fire. is
regarding the preparation of the altar and
Nachiketas being eager to learn, listened with whole-
hearted attention and was able to repeat all that was told
him. This so pleased Yama that he granted him the extra
boon of naming the fire-sacrifice after
him and gave him a
garland set with precious stones.
Verses XVI-XVIII are regarded by many as an inter-
polation, which would account for certain obscurities and
repetitions in them. XX XTACHIKETAS
-L ll death. said: There
regarding what becomes Some say he
does not exist. is of a
this doubt man after exists, others that he
This knowledge I desire, Of the boons this
being instructed by thee. is the third boon.
XXI replied: YAMA Ones) It is
of me. the Devas (Bright
doubted regarding this. not easy to know; subtle indeed is
O Nachiketas, choose another not press me. Ask not this boon
this subject. boon. Even Do
of oldThe Upanishads 48 XXII
said: O Death, thou NACHIKETAS
sayest that even the Devas had doubts
about this, and that it is not easy to know.
Another teacher like unto thee is not to be
found. Therefore no other boon can be
equal to this one. XXIII
A Ask and grandsons hundred years, many
Ask for cattle, elephants, gold and horses.
lands of vast extent and live thyself as many
YAM who autumns said: for sons
shall live a as thou desirest.
XXIV thou thinkest of any other boon equal
this, ask for wealth and long life; be
IF to ruler over the wide earth.
shall make thee enjoyer of
O Nachiketas, I all desires.
XXV objects of desire are
in the realm of WHATSOEVER
difficult to obtain mortals, ask
lovely them maidens all as thou desirest;
with their chariots these andKatha- Upanishad
49 musical instruments, such as are not ob-
tainable by mortals be served by these
whom I give to thee.
Nachiketas, do not ask regarding death.
HpHE third boon asked by Nachiketas concerning the
great Hereafter was one which could be granted only
to those who were freed from all mortal desires and limit-
ations; Yama therefore first tested Nachiketas
to see whether he was ready to receive such knowledge. "Do
not press me regarding this secret," he said. "Even wise
men cannot understand it and thou
art a mere lad. Take,
wealth, whatever will give thee happiness
on the mortal plane." But the boy proved his strength
rather, long life, and worthiness by remaining firm
the great secret of life and death.
in his resolution to know
XXVI Death, these are lyTACHIKETAS said:
L^ fleeting; they weaken the vigor of all
the senses in short. man.
Even Keep thou thy the longest
chariots, life is dance and
music. XXVII MAN cannot be
Shall we thee (Death)? by wealth.
possess wealth when we see
Shall we continue to live as
satisfied long as thou rulest? Therefore that boon
alone is to be chosen by me.The Upanishads
5o XXVIII man dwelling on the decaying
mortal plane, having approached the
undecaying immortal one, and having re-
WHAT upon the nature of enjoyment through
beauty and sense pleasure, would delight in
flected long life? XXIX O
DEATH, that regarding which there
is doubt, of the great Hereafter, tell us.
Nachiketas asks for no other boon than that
which penetrates this hidden secret.part
said: The good YAMA the pleasant
is another. one thing and
These two, having different ends, bind a man. It is
He well with him who chooses the good.
who chooses the pleasant misses the true end.
II and the pleasant approach
the wise examines both and dis-
criminates between them; the wise prefers
THE man; good the good to the pleasant, but the foolish man
chooses the pleasant through love of bodily
pleasure. Ill after wise reflection
ONACHIKETAS, thou hast renounced the pleasant and
hast not accepted this garland of great value for which many
mortals perish. all pleasing forms.
ThouThe 52 U pants hads
IV WIDE and apart are these two,
what in leading many ignorance
as directions. opposite Nachiketas to be one
since known is who tempting
wisdom, I believe longs for wisdom,
objects have not turned thee aside.
7ITH "\ 11 this second part, the Ruler of Death begins his
instructions regarding the great Hereafter.
There * * are two paths,
one leading Godward, the other leading
He who follows one inevitably goes
to worldly pleasure. away from
they the other; because, like light and darkness,
One leads to the imperishable spiritual
conflict. realm; the other to the perishable physical realm. Both
confront a man at every step of life. The discerning man,
distinguishing between the two, chooses the Real and
Eternal, and he alone attains the highest;
while the ignorant man, preferring that which brings him immediate
and tangible results, misses the true purpose of his exist-
Although Yama put before Nachiketas many
temptations to test his sincerity and earnestness, he,
ence. judging them at their real value, refused them all, saying:
"I have come from the mortal realm, shall I ask for what
is mortal? Death I which is eternal." Then
"I now see that thou art a sincere
desire only that said to him:
desirer of Truth. and every form
I offered thee vast wealth, long
of pleasure life which tempts and deludes men;
but thou hast proved thy worthiness by rejecting them
all."Katha-Upanishad 53 dwelling in ignorance, yet imagin-
wise and learned, go FOOLS
ing themselves round and round in crooked ways,
blind led by like the
the blind. VI THE luded
never Hereafter rises before the
(the ignorant), de- thoughtless the
"This by glamour of wealth.
child world alone is, thus, he falls
'TpHERE * are there under
many is none other ": thinking
my sway in the world,
again and again. who, puffed up with
intellectual conceit, believe that they are capable of
guiding others. amount But although they may possess a
certain wisdom, they are devoid of deeper
understanding; therefore all that they say merely increases
doubt and confusion in the minds of those who hear them.
of worldly Hence they are likened
men leading the blind. shine before those who are
to blind The Hereafter does not
lacking in the power of discrimination and are easily
carried away therefore by the charm of fleeting objects
As children are tempted by toys, so they arc tempted by
pleasure, power, name and fame.
To them these seem Being thus attached to perishable
things, they come many times under the dominion of
death. There is one part of us which must die; there is
the only realities.The Upanishads 54
another part which never dies. When a
himself with his undying nature, which
then he overcomes death. man
is can identify one with God,
VII HE whom many are not even able
hear, whom many cannot compre-
about to hend even after hearing:
he wonderful who can teacher, wonderful is
when taught by an able teacher.
is the receive the Vedic Scriptures it is declared
that no one can impart spiritual knowledge unless
'-pHROUGHOUT * he has realization.
What means knowledge based on
is meant by realization? direct perception.
It In India often the best teachers have no learning, but their character
is so shining that every one learns merely by coming in
In one of the Scriptures we read:
contact with them. Under a banyan tree sat a youthful teacher and beside
him an aged disciple. The mind of the disciple was full of
doubts and questions, but although the teacher continued
silent, mind. gradually every doubt vanished from the disciple's
This signifies that the conveying of spiritual teach-
ing does not depend upon words only. It is the life, the
Such God-enlightened men, illumination, which counts.
however, cannot easily be found;
but even with such a
teacher, the knowledge of the Self cannot be gained unless
the heart of the disciple is open and ready for the Truth.
Hence Yama says both teacher and taught must be
wonderful.Katha- Upanishad 55 VIII man
taught by a understanding, of
inferior Atman cannot this be truly known, even though frequently
thought upon. There is no way (to know
It) unless it is taught by another (an il-
lumined teacher), for subtle it is
subtler than the and beyond argument.
IX O DEAREST, this Atman cannot be
attained by argument; It is truly
known only when taught by another (a wise
teacher). It. Thou O Nachiketas, thou hast attained
May we ever art fixed in Truth.
find a questioner like thee.
of the Atman or Self cannot be attained
T^NOWLEDGE '* when it is taught by those who themselves lack in
understanding of It; and who therefore, having no
definite conviction of their own, differ among themselves
real as to its nature and existence.
Only he who has been
able to perceive the Self directly, through the unfoldment
can proclaim what It actually is; and
words alone carry weight and bring illumination. It
is too subtle to be reached by argument.
This secret regarding the Hereafter cannot be known through reason-
It is to be attained
ing or mere intellectual gymnastics.
of his higher nature, his
only in a state of consciousness which transcends the
boundary line of reason.The Upanishads
56 X KNOW that (earthly) treasure is transi-
I tory, for the eternal can never be attained
by things which are non-eternal. Hence the
Nachiketa fire (sacrifice) has been performed
by me with perishable things and yet I have
attained the eternal. XI thou hast seen the
ONACHIKETAS, filment of all desires,
ful- the basis of the
universe, the endless fruit of sacrificial rites,
the other shore where there is no fear, that
praiseworthy, the great and wide
support; yet, being wise, thou hast rejected
all with firm resolve. which
is 'T^HE teacher, saying that the imperishable cannot be
attained by the perishable, shows that no amount of
observance of rituals and ceremonies can earn the im-
* perishable and eternal. Although
the Nachiketa fire- bring results which seem eternal to mortals
because of their long duration, yet they too must come
sacrifice to may an end;
final goal. therefore this sacrifice cannot
Yama lead to the praises Nachiketas because,
when all heavenly and earthly pleasures, as well as knowledge of
all realms and their enjoyments were offered him, yet he
cast them aside and remained firm in his desire for Truth
alone,Katha- Upan is had 57
XII THE wise, who by means
meditation on the of the highest
Self knows the Ancient One, difficult to perceive, seated in
the innermost recess, hidden in the cave of
the heart, dwelling in the depth of inner being,
(he who knows that One) as God, is liberated
from the and sorrow. fetters of joy
XIII A MORTAL, grasped heard
having this, and and having
fully realized through discrimination the subtle Self, re-
joices, because he has obtained that which is
the source of Truth) is
I think the all joy.
open abode (of to Nachiketas.
'"THE Scriptures give three stages in all spiritual attain-
^ ment. The aspirant must first hear about the
Truth from an enlightened teacher; next he must reflect
upon what he has heard; then by constant practice of
discrimination and meditation he realizes it; and with
comes the fulfilment him with the source of
realization it unites of every desire, because
all. Having beheld this, man
learns that all sense pleasures are but fragmentary
reflections of that one supreme joy, which can be found in
a the true Self alone.
is no doubt Yama assures Nachiketas that there
of his realizing the Truth, because he has shown
the highest discrimination as well as fixity of purpose.The Upaniskads
58 XIV lyTACHIKETAS -L^l which
said: That which them neither virtue nor vice,
seest, neither cause nor effect, neither past nor
is future (but beyond these),
tell me That. XV That goal which all the
all austerities pro- which glorify,
which claim, desiring (people) practise Brah-
macharya (a life of continence and service),
replied: YAMA Vedas that goal I
tell T T 7HAT name
* * thee briefly can
man give to it is
God? Aum. How can the
be bound by any finite word? All that
language can express must be finite, since it is itself finite.
Yet it is very difficult for mortals to think or speak of any-
Infinite thing without calling it by a definite name.
this, the Sages gave to the Supreme the name
Knowing A-U-M, language. The first letter
"A" is the mother-sound, being the natural sound uttered
by every creature when the throat is opened, and no
which stands as the root of
all sound can be made without opening the throat. The last
letter "M," spoken by closing the lips, terminates all
articulation. As one carries the sound from the throat
to the lips, it passes through the sound "U." These three
sounds therefore cover the whole field of possible articulate
sound. Their combination is called the Akshara or
the imperishable word, the Sound-Brahman or the
Word-Katha-U panishad God, because the most universal
it is 59 name which can be
given to the Supreme. Hence it must be the word which
was "in the beginning" and corresponds to the Logos of
Christian theology. It is because of the all-embracing
name that it is used so universally in
the Vedic Scriptures to designate the Absolute.
significance of this XVI T
HIS Word is indeed Brahman. This
Word is indeed the Supreme. He who
knows this Word obtains whatever he desires.
XVII is THIS highest Support
'TpHIS * is glorified in the
realizes the world of Brahman.
Word is the highest symbol of
He who through meditating on It
full significance, all Support; is
he who knows this sacred
lute. has the best Support, This
the Abso- grasps Its and at once
the glory of God because God is the fulfilment
his desires satisfied, of all desires.
XVIII Self is THIS It did
never born, nor does It
die. not spring from anything, nor
did anything spring from It. This Ancient
One unborn, eternal, everlasting. It
slain even though the body is slain.
is is not60 The Upanishads
XIX the slayer thinks that he slays, or if the
thinks that he is slain, both of these
IF slain know not. For
It neither slays nor is
It slain. XX Self subtler than the subtle,
HpHE A greater than the great; It dwells in the
is heart of each living being.
He who is free from desire and free from grief, with mind
and senses tranquil, beholds the glory of the
Atman. A LTHOUGH this Atman
^^ living being, yet It
dwells in the heart of every
not perceived by ordinary mortals because of Its subtlety. It cannot be perceived
by the senses; a finer spiritual sight is required. The
heart must be pure and freed from every unworthy selfish
is desire; objects; the thought must be indrawn from
mind and body must be under
all external control; when the whole being thus becomes calm and serene, then it is
It is subtler possible to perceive that effulgent Atman.
than the subtle, because It
is the invisible essence of
every thing; and It is greater than the great because It is
the boundless, sustaining power of the whole universe;
that upon which all existence rests.Katha-U panishad
61 XXI THOUGH sitting, It travels far;
lying, It goes everywhere. save
me is fit joyful and
to know that God, though
Who who is else (both)
joyless? hence It is that which sits
and that which travels, that which is active and
It is both stationary and moving,
that which is inactive. and It is the basis of all forms of existence; therefore
whatever exists in the universe, whether joy or joylessness,
pleasure or pain, must spring from It. Who is better able
'HpHE * Self is all-pervading,
still I myself, since He resides in my heart
the very essence of my being? Such should be the
attitude of one who is seeking.
to know God than and
is XXII wise THE who know
the Self, bodiless, seated within perishable bodies, great
and all-pervading, grieve WHEN a wise
man not. through the practice of discrimi-
nation has seen clearly the distinction between
body and Soul, he knows that his true Self is not the body,
though It dwells in the body. Thus realizing the inde-
structible, all-pervading nature of his real Self,
mounts all fear of death or loss,
the greatest sorrow. and is
not he sur- moved even byThe Upanishads
62 XXIII Ft ^HIS A
Self cannot be attained by study
of the Scriptures, nor by
intellectual perception, nor by frequent hearing (of It);
He whom the Self chooses, by him alone is It
To him attained. the Self reveals Its true
nature. WE may imagine that by
much study we can find out
God; but merely hearing about a thing and gaining
an intellectual comprehension of it does not mean attain-
Knowledge only comes through ing true knowledge of it.
direct perception, and direct perception of God is possible
for those alone who are pure in heart and spiritually
awakened. Although He is alike to all beings and His
mercy is on all, yet the impure and worldy-minded do not
get the blessing, because they do not know how to open
He who longs for God, him the Lord
their hearts to it. chooses; because to him alone can He reveal His true
nature. XXIV HE who who has not turned away from
evil conduct, whose senses are uncontrolled,
is not tranquil, whose mind is not at rest,
he can never attain this Atman even by
knowledge.Katha- Upanishad VTAMA, * having
how first described what the
63 Atman is, now A man
must try to subdue his lower nature and gain control over the body and senses.
He must conquer the impure selfish desires which now
disturb the serenity of his mind, that it may grow calm
and peaceful. In other words, he must live the life and
develop all spiritual qualities in order to perceive the Atman.
tells us to attain It.
XXV WHO then can Self?
know where is this He
to (that Self) mighty Brdhmanas and Kshatriyas are but
food and death itself a condiment.
whom 'T^HIS ^ the text
Supreme. proclaims the glory and majesty of the
The Brdhmanas stand for spiritual
strength, the Kshatriyas for physical strength, yet both
are overpowered by His mightiness. Life and death alike
Him. As the light of the great sun swallows
the lesser lights of the universe, similarly all worlds
are lost in the effulgence of the Eternal Omnipresent
are food for up all
Being.Part Ctrfrfc i are two
who enjoy THERE good deeds
their the fruits of in the world,
having entered into the cave of the heart, seated
The knowers (there) on the highest summit.
of Brahman call them shadow and
light. So by householders who
fire-sacrifices or three Nachi- five
perform keta fire-sacrifices. also (they are called)
T TERE * * self,
The the two signify the Higher Self and the lower
dwelling in the innermost cave of the heart.
Seers of Truth, as well as householders
who follow the path of rituals and outer forms with the hope of enjoy-
ing the fruits of their good deeds, both proclaim that the
Higher Self is like a light and the lower self like a shadow.
When the Truth shines clearly in the heart of the knower.
then he surmounts the apparent duality of his nature and
becomes convinced that there is but One, and that all
outer manifestations are nothing but reflections or pro-
jections of that One. II
MAY we be keta which
is a bridge who perform sacrifice. May we
know the One, who is the highest im-
for those also able to learn that Nachi-
fire-sacrifice,Katha- Upanishad perishable Brahman for those
65 who cross over to the other shore which
desire to is beyond fear.
npHE significance of this text
is: May we acquire the
knowledge of Brahman, the Supreme, in both mani-
He is manifested as the
fested and unmanifested form. Lord
He of sacrifice for those
who follow the path of ritual.
the unmanifested, eternal, universal Supreme Being
The "other for those who follow the path of wisdom.
is shore," being the realm of immortality,
fear; because disease, death, and
fear, cease to exist there.
It is all is said to be
beyond that which mortals believed
by many that these two opening verses were a later interpolation.
Ill KNOW the Atman (Self) as the lord of
the chariot, and the body as the chariot.
Know also the intellect to be the driver and
mind the reins. IV '
I A HE senses are called the horses;
the sense objects are the roads; when the
Atman is united with body, senses and mind,
then the wise call Him the enjoyer.
-A T N Yama defines what part of our being
and what part is deathless, what is mortal and what
is immortal. But the Atman, the Higher Self, is so entirely
beyond human conception that it is impossible to give a
* the third chapter diesThe Upanishads
66 Only through similies can some idea
That is the reason why all the great
direct definition of It. of It be conveyed.
Teachers of the world have so often taught in the fcrm of
So here the Ruler of Death represents the Self
parables. as the lord of this chariot of the body. The intellect or
discriminative faculty is the driver, who controls these
by holding firmly the reins of the
The roads over which these horses travel are made
wild horses of the senses
mind. up which attract or repel the
of all the external objects
the sense of smelling follows the path of sweet
odors, the sense of seeing the way of beautiful sights.
Thus each sense, unless restrained by the discriminative
senses: faculty, seeks to the Self
is go out towards joined with body,
who is called who wills,
without discrimination and whose mind
his senses are is senses, It
the intelligent enjoyer; because It is the one
feels, perceives and does everything.
HE When its special objects.
mind and is always uncontrolled,
unmanageable, like the vicious horses of a driver.
VI he who BUT whose
is full mind of discrimination
and always controlled, his senses are manageable, like the good horses
of a driver. isKatha-U panishad
'T^HE man * who whose
fails not discriminative and to distinguish right from wrong, the real
from the unreal, desires, just as
intellect is 67 is away by his sense passions and
carried away by vicious horses
control. But he who clearly dis-
carried a driver over which he has lost
is tinguishes what is good from what is merely pleasant, and
controls all his out-going forces from running after apparent
momentary- pleasures, his senses obey and serve him as
good horses obey their driver.
VII who does not possess discrimination,
whose mind is uncontrolled and al-
HE ways impure, he does not reach that goal, but
falls again into Samsdra (realm of birth and
death). VIII he who possesses right discrimina-
tion, whose mind is under control and
always pure, he reaches that goal, from
BUT which he is not born again.
IX THE man who intellect
trolled mind for has a
discriminative the driver, and a con-
reaches the end for the reins,
Vishnu and Unchangeable One). (the All-pervading
of the journey, the highest place ofThe Upanishads
68 A DRIVER ** the road;
must possess first a thorough knowledge of
next he must understand how to handle
Then will he drive safely
Similarly in this journey of life, our
senses must be wholly under the control of our
the reins and control his horses.
to his destination. mind and
higher discriminative faculty; for only when all our forces
the abode work in unison can we hope to reach the goal
of Absolute Truth. X the
senses BEYOND beyond the objects
the is mind is are
the objects, the mind, beyond
the intellect, beyond the intellect
is the great Atman. XI
the great Atman is the
Un- BEYOND manifested; beyond the Unmanifested
is the Purusha (the Cosmic Soul);
beyond the Purusha there is nothing.
That is the end, that
is the final goal. TN
these two verses the Teacher shows the process of
discrimination, by which one attains knowledge of the
subtle Self. Beginning with the sense-organs, he leads up
to the less and less gross, until he reaches that which is
* subtlest of all, the true Self of man.
The senses are dependent on sense-objects, because without these the
senses would have no utility.
Superior to sense-objects is the mind, because unless these objects affect the mind, theyKatha-U panishad
Over the mind the deter-
cannot influence the senses. minative
is faculty Self faculty undifferentiated creative
Avyaklam; and above Than this is the
determinative this power; governed by the individual
the is exercises 69 Self;
beyond this energy known as
Purusha or Supreme nothing higher. That
Highest Abode of Peace and Bliss.
this there is is Self.
the goal, the XII Atman
(Self), hidden in THIS does not shine forth;
subtle seers through keen but It
all beings, seen by and subtle under-
is standing. TF * It dwells in all living beings,
why do we Because the ordinary man's vision
tracted. It is visible to those alone
is not see It? too dull and dis-
whose intellect has been purified by constant thought on the Supreme, and
whose sight therefore has become refined and sharpened.
This keenness of vision comes only when all our forces
have been made one-pointed through steadfast practice
of concentration and meditation. XIII
A WISE man (the intellect, intellect
by and that by the Peaceful
Paramdtman or Supreme Self). the great Atman,
One should control speech by
mind, mind byThe Upanishads yo
T TERE Yama * ^
if gives the practical method to be followed
realize the Supreme. The word
one wishes to "speech" stands
man must the mind the senses.
for all First, therefore, a
control his outgoing senses by the mind. Then
must be brought under the control of the dis-
must be withdrawn from on non-
The discriminative faculty in turn must
essential things. be controlled by the higher individual intelligence and
this must be governed wholly by the Supreme Intelligence.
criminative faculty; all that is,
and cease sense-objects it to waste its energies
XIV AWAKE! Having reached Ones (illumined Teachers),
gain understanding. The path is as sharp
as a razor, impassable and difficult to travel,
ARISE! the Great so the wise declare.
'TpHIS is the eternal call of the wise:
Awake from the slumber of ignorance! Arise and seek out those who
know the Truth, because only those who have direct vision
Truth are capable of teaching It. Invoke their blessing
with a humble spirit and seek to be instructed by them.
of The path is very
strong, wakeful No difficult to tiead.
lethargic person can safely travel
on it. thoughtless or One must be
and persevering. XV KNOWING touchless,
That which is soundless, also
formless, undecaying; tasteless, odorless, and
eternal; beginningless,Katba- Upanishad endless 71
and immutable; beyond the Un-
(knowing That) man escapes manifested:
from the mouth 'HpHE *
of of death. Ruler of Death defines here the innermost essence
our being. cannot be heard or
It nary object. Because of
felt never its extreme subtlety, it
or smelled or tasted like any ordi-
dies. It has no beginning or end.
unchangeable. Realizing this Supreme Reality, man
escapes from death and attains everlasting life. Thus
It is the Teacher has gradually led Nachiketas to a point where
he can reveal to him the secret of death. The boy had
thought that there was a place where he could stay and
become immortal. But Yama shows him that immor-
tality is a state of consciousness and
man clings to name and form, or
What dies? Form. Therefore the
as is not gained so long
to perishable objects. formful man
dies; but not that which dwells within.
subtle, similies Although inconceivably the Sages have always made an effort through
and analogies to give some idea of this inner Self
God within. They have described It as beyond
mind and speech; too subtle for ordinary perception,
but not beyond the range of purified vision.
or the XVI THE intelligent
man, who has heard and
repeated the ancient story of Nachi-
by the Ruler of Death, is glorified
ketas, told in the world of Brahman.The Upaniskads
72 XVII HE who with devotion
sembly of secret recites this highest
of immortality before Brdhmanas (pious men)
an as- or at the
time of Shrdddha (funeral ceremonies), gains
reward, he gains everlasting everlasting
reward.jFourtb i THE created Self-existent
this reason the senses man
sees out-going; the external, but not the inner Atman (Self).
Some wise man, however, desiring immor-
tality, for with eyes turned away
external) sees the Atman (from
the within. TN the last chapter the Ruler of Death instructed Nachi-
ketas regarding the nature and glory of the Self.
Now he shows the reason why the Self is not seen by the majority.
* It is because man's mind
is constantly drawn outward thiough the channels of his senses, and this prevents his
seeing the inner Self (Pratyagdtmari); but now and then
a seeker, wiser than others, goes within and attains the
vision of the undying Self.
II /CHILDREN ^^ (the ignorant)
pursue ex- ternal pleasures; (thus) they fall into
the wide-spread snare of death.
But the wise, knowing the nature of immortality, do
not seek things. the permanent among
fleetingThe Upanishads 74 'TpHOSE who
* and fail to and unreal, the fleeting and
hearts on the changeable things
are devoid of discrimination distinguish between real
the permanent, set their of this world;
hence they entangle themselves in the net
which leads inevitably to disappoint-
of insatiable desire, ment and
suffering. To such, death must seem a
reality; because they identify themselves with that which is born
and which dies. But the wise, who see deeper into the
nature of things, are no longer deluded by the charm of the
phenomenal world and do not seek for permanent happi-
ness among its passing enjoyments.
Ill which one knows form, taste,
touch and sense enjoy- THAT by sound,
smell, ments, by That also one knows whatever
remains (to be known). This verily is That
(which thou hast asked to know).
IV which a mortal perceives, both
in waking, by knowing by
THAT dream and in that great all-pervading
Atman the wise man grieves no more.
TN make plain that all
sense perception, in every is
sleeping, dreaming or waking these verses the teacher tries to
knowledge, as well as state of consciousness
all possible only because the Self exists.
There can be noKatba-Upanishad 75
knowledge or perception independent of the Self. Wise
men, aware of this, identify themselves with their Higher
Self and thus transcend the realm of grief.
V HE who knows Atman, the honey-
and (perceiver enjoyer of ob-
the lord ever as of the past and
near, jects), This verily is That.
future, fears no more. this
eater VI HE who Him
sees born seated in the five
of Tapas (fire of Brahman), born before water; who, having
elements, entered the cave of the heart, abides therein
this verily is ' That.
He, the Great Self, is the cause
According to the Vedas, His
first manifestation was Brahma, the Personal God or
Creator, born of the fire of wisdom. He existed before the
evolution of the five elements
earth, water, fire, air and
He is the Self ether; hence He was "born before water."
I ''HIS verse indicates that
of all created objects. dwelling in the hearts of
all creatures. VII who knows Aditi, who rises with
Prana (the Life Principle), existent in
the Devas; who, having entered into the
HE allThe Upanishads 76 and who was born from
heart, abides there; the elements
T HIS verse this verily is
is That. somewhat obscure and seems
like an interpolated amplification of the preceding verse.
VIII THE all-seeing fire two
in the guarded in the
which exists hidden sticks, as the foetus is well-
womb by the mother, (that
to be worshipped day after day by
wakeful seekers (after wisdom) as well as by
This verily is That. sacrificers.
fire) is , is called all-seeing because its light makes every-
In Vedic sacrifices the altar fire was
thing visible. always kindled by rubbing together two sticks of a special
kind of wood called Arani.
Because fire was regarded as
one of the most perfect symbols of Divine wisdom, it was
to be worshipped by all seekers after Truth, whether they
followed the path of meditation or the path of rituals.
IX and whither That all the
upon one goes beyond That.
whence the sun FROM goes
it at setting, Devas depend.
This verily is No That.
rises,Katba- Upanishad 77 X WHAT
that who sees is here
is there (in the visible world),
the (in invisible); he (between visible and
from death to death. difference
invisible) goes XI mind alone
BY There tween is visible
no and difference here this
is difference invisible). be realized.
to whatever (be- He who
sees (between these) goes from
death to death. T N
the sight of true wisdom, there is no difference between
and the created. Even physical science has
the creator come of to recognize that cause
and one manifestation of energy.
effect are but two aspects
He who fails to see this,
being engrossed in the visible only, goes from death to
death; because he clings to external forms which are per-
Only the essence which dwells within is unchange-
and imperishable. This knowledge of the oneness of
visible and invisible, however, cannot be acquired through
It can only be attained by the purified
sense-perception. ishable. able mind. XII
Purusha (Self), of the size of a
thumb, resides in the middle of the
body as the lord of the past and the future,
AThe Upanishads 78 (he who knows Him)
fears no more. This verily is That.
npHE seat of the Purusha
is said to be the heart, hence It
"resides in the middle of the body."
is limitless and all-pervading, yet
Although It in relation to Its abiding-
place It is represented as limited in extension, "the size of
a thumb." This refers really to the heart, which in shape
As light is everywhere, yet
focused in a lamp and believe it to be there only;
similarly, although the life-current flows everywhere in
may we be likened to a thumb.
see it the body, the heart
is regarded as peculiarly its seat.
XIII THAT Purusha, of the size of a thumb,
a light without smoke, lord of
is like the past and the future.
today and tomorrow. TN He
is the This verily is
same That. this verse the teacher defines the effulgent nature of
the Soul, whose light is pure like a flame without smoke.
He also answers the question put by Nachiketas as to what
happens after death, by declaring that no
takes place, because the Soul
is real change ever the same.
XIV \ *^- S rain water, (falling) on the mountain
top, runs down over the rocks on all
sides; similarly, he who sees
differenceKatba- Upanishad 79 (between visible forms) runs after them in
various directions. XV O GAUTAMA (Nachiketas), as pure water
also poured into pure water becomes one, so
is it with the Self of an illumined Knower
(he becomes one with the Supreme).Part
THE city of the edge
is Unborn, whose knowl- unchanging, has eleven gates.
man grieves no more; freed (from ignorance), he attains
Thinking on Him, and being
liberation. ' I A This verily
HIS human body is is
That. called a city with eleven gates,
where the eternal unborn Spirit dwells. These gates
are the two eyes, two ears, two nostrils, the mouth, the
navel, the two lower apertures, and the imperceptible
opening at the top of the head.
The Self or Atman holds
the position of ruler in this city; and being above the
modifications of birth, death and all human imperfections,
It is not affected by the changes of the physical organism.
intelligent man through constant thought and
As the meditation realizes the splendor of this Supreme Spirit,
free from that part of his nature which grieves
he becomes and suffers, and thus he attains
liberation. II the sun dwelling in the bright
heaven; He is the air dwelling in
space; He is the fire burning on the altar;
He is the guest dwelling in the. house. He
HE is dwells in man.
He dwells in those greaterKatha- Upanishad
man. than He dwells in
8 1 sacrifice. dwells in the ether. He
in water, (all that) born in earth,
is born in mountains. is
sacrifice, He is (all is (all
that) is) (all that) that
is He born born on
the True and the Great.
Ill who sends the (in-coming) Prana
(life-breath) upward and throws the
HE it is Him all the
(out-going) breath downward. senses worship, the adorable Atman, seated
in the centre (the heart).
IV WHEN what this Atman, which
is seated in the body, goes out (from the body),
remains then? This verily is That.
V mortal lives by the in-coming breath
(Prana) or by the out-going breath
NO (Apdna), but he lives by another on which
these two depend.The Upanishads 82
VI O GAUTAMA clare (Nachiketas), I shall de-
unto thee the secret of the eternal
Brahman and what happens to the Self after
death. VII Jivas SOME wombs
to (individual Souls) enter be embodied; others go into
to their deeds immovable forms, according
and knowledge. ' I ''HIS text shows the application of the law of cause and
effect to all forms of life.
The thoughts and actions of the present
life determine the future birth and environ-
ment. VIII ' I ^HE
Being who remains awake while all
sleep, who grants all desires, That is
-- pure, That is be immortal.
Brahman, That alone is said to
On That all the worlds rest.
None goes beyond That. This verily
is That. IX AS what
fire, though one, having entered the
world, becomes various according to
it burns, so does the Atman
(Self)Katha- Upanishad within all living
83 beings, though one, various according to
what it enters. become It also
exists outside. X though one, having entered the
world, becomes various according to
AS what air, it enters, so does the
Atman within all living beings, though one, become various
according to what it enters. It also exists
outside. t> Y using these similies of fire and air, the teacher tries
show Nachiketas the subtle quality of the great
Self, who, although one and formless like air and fire, yet
assumes different shapes according to the form in which
to It dwells. But, being all-pervading and unlimited, It
cannot be confined to these forms; therefore it is said that
It also exists outside all forms.
XI the sun, the eye of the whole world, is
not denied by external impurities seen
the eyes, thus the one inner Self of all
AS by living beings is
not denied by the misery of
the world, being outside '
I ^HE all sun is
it. called the eye of the world because
objects. As the sun may
shine on impure object, yet remain uncon laminated by
it reveals the most it,
so theThe Upanishads 84 Divine Self within
not touched by the impurity or
form in which it dwells, the Self
is suffering of the physical
being beyond bodily limitations. all
XII is one ruler, the Self of all living
THERE beings, who makes the one form mani-
wise the fold; within their
bliss, who to Self, Him seated
them belongs eternal perceive not to others.
XIII the among changing, con-
ETERNAL sciousness of the conscious, who, though
one, fulfils the desires of
many : the wise who
Him seated within their Self, to
perceive them belongs eternal peace, not to others.
XIV THEY (the scribable is
That. wise) highest How am
shine (by Jts own reflected light)?
perceive I to bliss, know
that inde- saying, This It?
Does It light) or does It shine (byKatba-U panishad
85 XV sun does not shine there, nor the
nor the stars; nor do these
THE moon, lightnings shine there,
When He shines, Him; by His
much less this fire. everything shines after
light all is lighted.Part ancient Aswattha tree has
THIS above pure, That goes
root and branches below. That is
Brahman, That alone is called
is the Immortal. None its
All the worlds rest in That.
beyond That. This verily is
That. ' I ''HIS verse indicates the origin of the tree of creation
(the Samsdra-Vriksha), which is rooted above in
Brahman, the Supreme, and sends its branches downward
Heat and cold, pleasure and
into the phenomenal world. pain, birth and death, and all the shifting conditions of the
mortal realm the tree, the
and these are the branches
Brahman, deathless. From is ;
but the origin of eternally pure, unchanging, free
the highest angelic form to the
minutest atom, all created things have their origin in Him.
He is the foundation of the universe. There is nothing
beyond Him. II there WHATEVER
evolved from is in the universe
Prdna and vibrates in Prana.
That is a mighty terror, like an
upraised thunderbolt. They who know That
become immortal. is87 Katba-Upanishad III
FROM of Him Him fear of
fear of Him the fire
the sun shines. burns, from
From Indra and Vayu and Death, the
fear fifth, speed forth. JUST
as the body cannot live or act
without the Soul, similarly nothing in the created world can exist inde-
pendent of Brahman, who is the basis of all existence.
His position is like that of a king whom all must obey;
hence it is said that the gods of sun, moon, wind, rain, do
His bidding. He is likened to an upraised thunderbolt,
because of the impartial and inevitable nature of His law,
which all powers, great or small, must obey absolutely.
IV man a IF the
comes A S soon as a
he is not able to
know Him before dissolution of the body, then he be-
embodied again in the created worlds.
is man acquires knowledge of the Supreme,
if he fails to attain such knowledge
liberated; but is separated from the body, then he must
take other bodies and return again and again to this realm
before his Soul of birth
and death, realizes the until through varied experience he
nature of the Supreme and his relation to Him.The Upanishads
AS a mirror, so is He seen within
oneself; as in a dream, so (is He seen)
in in the world of the fathers (departed spirits)
as in water, so (is He seen) in the world of
; Gandharoas and (the shadow, so
Brahma angelic (is He As
realm). light world of seen) in the
(the Creator). VIT'HEX by means
beholds God polished mirror; of a purified
understanding one within, the image
is distinct as in a
but one cannot have clear vision of the
Supreme by attaining to the various realms
known as heavens, where one reaps the fruit of his good deeds. It
is only by developing one's highest consciousness here in
this life that perfect God-vision
can be attained. VI that the senses are distinct
KNOWING (from the Atman) and their rising and
setting separate (from the Atman}, a wise
man A !*^ grieves no more.
WISE man birthless and end.
and never confounds the Atman, which is
which has beginning deathless, with that
Therefore, when he sees his
physical organism waxing and waning, he
real Self within can never be affected
changes, so he remains unmoved.
and knows that senses by
his his these outerKatha- Upanishad
89 VII than the senses
HIGHER higher than the mind
is the mind, the intellect,
is higher than the intellect is the great Atman,
higher than the Atman is the Unmanifested.
VIII the Unmanifested BEYOND pervading
(Purusha). liberated ' I By
is the all- and imperceptible Being
knowing Him, the mortal is
and attains immortality. *HIS division
the of individual into senses,
mind, intellect, self-consciousness, undifferentiated creative
energy and the Absolute Self is explained in the com-
mentary of verse XI, Part Third.
IX HIS form see ceived
is not to be seen.
Him with the eye. No one can
He is per- by the heart, by the intellect and by
They who know this become
the mind. immortal. r*HE Supreme, being formless, cannot be discerned by
the senses; hence all knowledge of Him must be
acquired by the subtler faculties of heart, intellect and
mind, which are developed only through the purifying
practice of meditation.The Upanishads 90
X the WHEN become and the
organs of perception together with the mind,
five still, intellect ceases to
be active : that is
called the highest state. ~*HE teacher now shows Nachiketas the process by
which the transcendental vision can be attained.
The out-going seeing, hearing, smelling, touch-
senses, mind and the intellect: all must
be indrawn and quieted. The state of equilibrium thus
ing, tasting; the restless attained
is called the highest state, because all the forces
become united and focused; evitably leads to supersensuous vision.
of one's being and this in-
XI firm holding back of the senses is
Then one is known as Yoga.
THIS what should become watchful, for Yoga comes and
goes. literally means with the Higher
worshipper with God. one must
first to join or to unite the lower self
the object with the subject, the
Self, In order to gain this union, however,
all that scatters the disunite oneself from
physical, mental and intellectual forces;
so the outgoing perceptions must be detached from the external world and
indrawn. When this is accomplished through constant
practice of concentration place of its own accord.
one is watchful. and meditation, the union takes
But it may be lost again, unlessKatha- Upanishad
9 1 XII cannot be attained by speech, by
mind, or by the eye. How can That
HE be realized except by him
who "He is"? is" (visible and
and "He is," says XIII
HE should be realized as
also as the reality of
invisible). to "He both He who knows Him
him alone His real nature
is as revealed. T"*HIS supersensuous vision cannot be gained through
man's ordinary faculties. By mind, eye, or speech
the manifested attributes of the Divine can be apprehended;
but only one who has acquired the supersensuous sight
can directly perceive God's existence and declare definitely
that "He is," that He alone exists in both the visible and
the invisible world. XIV WHEN
dwelling in the heart the
mortal becomes all desires cease,
then immortal and attains Brahman here.
XV WHEN all the ties of the heart are cut
asunder here, then the mortal be-
comes immortal. Such is the teaching.The Upanishads
92 XVI r I ^HERE
JL and one nerves of
them penetrates the are a hundred
One the heart. centre of the head.
of Going upward through immortality. The other
nerve -courses) lead, in departing,
(hundred one it, attains to different worlds.
' I ''HE nervous system of the body provides the channels
through which the mind travels; the direction in
which it moves is determined by its desires and tendencies.
When the mind becomes pure and desireless, it takes the
upward course and at the time of departing passes out
through the imperceptible opening at the crown of the
head; but as long as it remains full of desires, its course is
downward towards the realms where those desires can be
satisfied. XVII THE Puruska, the inner
of a thumb, is Self, of
the size ever seated in the heart
With perseverance man should draw Him out from his body as one
of all living beings. draws the inner stalk from a blade of grass.
One should know Him as pure and deathless,
as pure and deathless. A S
has been explained in Part Fourth, verse XII, the
inner Self, although unlimited, is described as "the
size of a thumb" because of its abiding-place in the heart,Katha-U panishad
often likened to a lotus-bud which
size and shape. is 93
similar to a Through the process
thumb in of steadfast dis-
crimination, one should learn to differentiate the Soul from
the body, just as one separates the pith from a reed.
XVIII Nachiketas, having acquired this
THUS wisdom taught by the Ruler
together with free all of
the rules of Yoga, Death,
became from impurity and death and attained
So also will it be
(the Supreme). Brahman with another
who likewise knows the nature
of the Self. PEACE CHANT
MAY He (the Supreme Being) protect us
May He be pleased with us.
May we acquire strength. May our study
May there be no bring us illumination.
both. enmity among us. OM! PEACE! PEACE! PEACE!
Here ends this UpanishadKENA-UPANISHADthe Like the Isavasya, this Upanishad derives its name from
opening word of the text, Kena-ishitam, "by whom
directed." because of of the
It is also its known
as the Talavakdra-Upanishad place as a chapter in the Talavakdra-Brdhmana
Sdma-Veda. the Upanishads it is one of the most analytical
and metaphysical, its purpose being to lead the mind from
the gross to the subtle, from ejfect to cause.
By a series of profound questions and answers, it seeks to locate the source
of man's being; and to expand his self-consciousness until
Among it has become identical with God-Consciousness.KENA-UPANISHAD
IV/f AY -*-*-- all my
force), my limbs, sight, speech,
Prdna (life- hearing, strength and
All is the Brah- senses, gain in vigor.
man (Supreme Lord) of the
Upanishads. deny the Brahman. May the
Brahman never deny me. May there be no
denial of the Brahman. May there be no
May all the separation from the Brahman.
May I never virtues declared in the sacred Upanishads be
who am devoted to the
manifest in me, Atman (Higher
in Self). May they be manifest
me. OM! PEACE! PEACE! PEACE!part
BY whom commanded and directed does
the mind go towards its objects? Com-
manded by whom does the life -force, the
first (cause), move? At whose will do men
utter speech? What power directs the eye
and the ear? ' I
* HUS the disciple approached the Master and inquired
concerning the cause of life
and human activity. Hav- ing a sincere longing for Truth he desired to know who
really sees and hears, who actuates the apparent physical
He perceived all about him the
the existence of which he could prove
man. phenomenal world, by his senses; but
know the invisible causal world, of which he
was now only vaguely conscious. Is mind all-pervading
and all-powerful, or is it impelled by some other force,
he sought to he asked.
Who which nothing can sends forth the vital energy,
exist? The teacher replies: without
II the ear of the ear, the mind of the
the speech of the speech, the life of
the life, the eye of the eye. The wise, freed
is IT mind, (from the senses and from mortal desires),
after leaving this world, become immortal.Kena- Upanishad
A N ordinary to man
know only 99 hears, sees, thinks, but he
as much as can be
is satisfied known through the
he does not analyze and try to find that which
stands behind the ear or eye or mind. He is completely
His conception does identified with his external nature.
senses; not go beyond the little circle of his bodily life, which con-
He has no consciousness of
cerns the outer man only.
that which enables his senses and organs to perform their
tasks. There is a vast difference between the manifested form
and That which is manifested through the form. When
we know That, we shall
clings to the senses and
die many not die with the body.
One who must to things that are ephemeral,
deaths; but that man who knows
the eye of the eye, the ear of the ear, having severed himself
becomes from his immortal. Immortality is
when man transcends his apparent nature and
finds that subtle, eternal and inexhaustible essence which
physical attained is nature, within him.
Ill the eye does not go, nor speech,
THERE nor mind. We do not know That;
we do not understand how It can be taught.
It is distinct from the known and also It is
beyond the unknown. Thus we have heard
from the ancient (teachers) who told us
about A I A It.
HESE physical subtle essence. language or
eyes are Nor can known by
finite unable perceive that be expressed by
intelligence, because finite it to
it isThe Upanishads ioo infinite.
know Our conception of knowing finite
name and form; but knowledge
their far is to God must
why some from such knowledge. This is
unknown and unknowable; because He
more than eye or mind or speech can perceive, com-
be distinct declare is things
of God to be prehend or express. The Upanishad does not say that He
cannot be known. He is unknowable to man's finite
nature. How can a finite mortal apprehend the Infinite
Whole? But He can be known by man's God-like nature.
IV which speech does not illumine,
THAT but which alone to be the
illumines speech Brahman (the :
know that Supreme Being), not this which people worship here.
which cannot be thought by mind,
THAT but by which, to think:
man, not know this they say, mind is able
that alone to be the Brah-
which people worship here. VI
which THAT by which is
not seen by the eye, but
the eye is able to see know
that alone to be the Brahman, not this which
people worship here. :Kena-U panishad
101 VII which cannot be heard by the
THAT but by which the
ear is ear, able to hear:
know that alone to be Brahman, not
which people worship here. this
VIII '"T^HAT which none breathes with
the A breath, but by which breath is in-
breathed: know that alone to be the Brah-
man, not this /^RDINARILY ^"^
which people worship here. we know
three states of consciousness waking, dreaming and sleeping.
only, There is, however, a fourth state, the superconscious, which tran-
In the first three states the mind is not
scends these. clear enough to save us from
error; but in the fourth state
gains such purity of vision that it can perceive the
Divine. If God could be known by the limited mind and
it senses, then God-knowledge would be
like any other knowledge and spiritual science like any physical science.
He can be known, however, by the purified mind only.
Therefore to know God, man must
mind described purify himself. The
Upanishads is the superconscious mind. According to the Vedic Sages the mind in its
ordinary state is only another sense organ. This mind is
able to behind when becomes illumined by the light of the
"mind of the mind," then it is
apprehend the First Cause or That which stands
limited, but Cosmic in the
it Intelligence, or the all
external activities.Part &econD i thou thinkest "I know It well," then
certain that thou knowest but little
it is of the Brahman
form He (Absolute Truth), or in what
(resideth) aspects of Deity). what thou thinkest
sought in the Devas (minor
Therefore I think that to be
known is still to be
after. TJTAVING given the definition of the real Self or Brah-
man, by which mortals are able
think, the teacher was afraid that the
hearing about said to him:
It, to see, hear, feel
disciple, after might conclude that he knew It.
heard about It, but that
"You have and merely So he
is not You must experience It. Mere intellectual
recognition will not give you true knowledge of It. Neither
can It be taught to you. The teacher can only show the
enough. You must find It for yourself."
Knowledge means union between subject and object.
To gain this union one must practise; theory cannot help
The previous chapter has shown that the knowledge
us. of Brahman is beyond sense-perception: "There the eye
does not go, nor speech, nor mind." "That is distinct
from known and also It is beyond the unknown." There-
fore it was necessary for the teacher to remind the disciple
that knowledge based on sense-perception or intellectual
way.Kena-Upanishad 103 apprehension should not be confounded with supersensuous
knowledge. Although the disciple had listened to the
teacher with unquestioning mind and was intellectually
it was now necessary experience what he had heard.
Guided by the teacher, he sought within himself through
meditation the meaning of Brahman; and having gained
convinced of the truth of his words,
for him new a to prove
vision, by his own he approached the teacher once more.
II THE disciple said: I
It well, nor do do not think
I think that I I
know do not It. He among us who knows It truly,
knows (what is meant by) "I know" and
also what is meant by "I know It not."
know T^HIS appears to be contradictory, but it is not.
In the previous chapter we learned that Brahman is "dis-
tinct from the known" and "beyond the unknown."
The disciple, realizing this, says: "So far as mortal conception
concerned, I do not think I know, because I understand
that It is beyond mind and speech; yet from the higher
point of view, I cannot say that I do not know; for the
is It, shows that I know;
do not know, however, in the sense of knowing the whole Infinite Ocean of exist-
ence." The word knowledge is used ordinarily to signify
acquaintance with phenomena only, but man must transcend
this relative knowledge before he can have a clear con-
ception of God. One who wishes to attain Soul-conscious-
ness must rise above matter.
very fact that I exist, that I
for It is the source of
can seek my being. IThe Upanishads
iO4 The observation of material science being confined to
ignores what is beyond. Therefore it
must always be limited and subject to change. It dis-
covered atoms, then it went further and discovered elec-
the sense plane, it and when
it had found the one, it had to drop the
so this kind of knowledge can never lead to the
ultimate knowledge of the Infinite, because it is exclusive
trons, other; and not inclusive.
Spiritual science is not merely a
question of mind and brain, it depends on the awakening
of our latent higher consciousness.
Ill not. who thinks he knows It not, knows It.
He who thinks he knows It, knows It
The true knowers think they can never
know It (because of Its infinitude), while the
HE ignorant think they O Y
know It. this text the teacher confirms the idea that
Brahman unthinkable, because unconditioned. Therefore
he says: He who considers It beyond thought, beyond
sense-perception, beyond mind and speech, he alone has a
is true understanding of Brahman.
They who judge a living
being from his external form and sense faculties, know him
not; because the real Self of
seeing, hearing, speaking. man His
not manifested in his is
real Self is that within
by which he hears and speaks and sees. In the same way,
he knows not Brahman who thinks he knows It by name
and form. The arrogant and foolish man thinks he knows
everything; but the true knower is humble. He says:
"How can I know Thee, who art Infinite and beyond mindKena-U panishad
105 and speech? " In the last portion of the text, the teacher
draws an impressive contrast between the attitude of the
wise man who knows, but
who that of the ignorant
thinks he does not know;
and does not know, but thinks he
knows. IV (Brahman) is known, when
It is known IT in every state of consciousness.
such knowledge) By (Through one attains immortality.
attaining this Self, man gains strength;
and by Self-knowledge immortality is
at- tained. have learned from the previous text that the
is unknown to those whose knowledge is
limited to sense experience; but He is not unknown to
those whose purified intelligence perceives Him as the
"V\7"E Brahman basis of all states of consciousness
and the essence of all
higher knowledge a man attains im-
mortality, because he knows that although his body may
decay and die, the subtle essence of his being remains
things. By untouched. this Such an one also acquires unlimited strength,
because he identifies himself with the ultimate Source.
The strength which comes from one's own muscle and
brain or from one's individual power must be limited and
mortal and therefore cannot lift
one beyond death; but through the strength which Atma-gndna or Self-knowledge
immortality is reached. Whenever knowledge is
based on direct perception of this undying essence, one
transcends all fear of death and becomes immortal.
gives,The Upanishads io6 V F
I one knows It here, that is Truth; if
one knows It not here, then great is his
loss. The beings, wise seeing the same Self in
being liberated become immortal. from
this all world,part CtJftD i
f I ^HE Brahman once won a victory for the
-- Devas. Through that victory of the
Brahman, the Devas became elated. They
thought, "This victory is ours. This glory
is ours." 13 RAHMAN is
mean a personal Deity. There
person of the Hindu Trinity;
the Absolute, the One without a second,
here does not a Brahma, the
but Brahman is first all.
There are different names and forms
which represent certain personal aspects of Divinity, such
as Brahma the Creator, Vishnu the Preserver and Siva the
Transformer; but no one of these can fully represent the
Whole. Brahman is the vast ocean of being, on which
rise numberless ripples and waves of manifestation.
From the smallest atomic form to a Deva or an angel, all spring
from that limitless ocean of Brahman, the inexhaustible
Source of life. No manifested form of life can be inde-
the essence of pendent of its source, just as no wave, however mighty,
can be independent of the ocean. Nothing moves without
that Power. He is the only Doer.
But the Devas thought: "This victory
is ours, this glory is
ours." II Brahman THE peared
before perceived this and ap-
them. They did not know what mysterious form
it was.The Upanishads io8 III
npHEY -I- said to Fire:
knowing) Jataveda (All- Find out what mysterious
! He spirit this is."
"O " said : Yes."
IV HE said to am
and He (Brahman) him: "Who art thou?" "I
ran towards Agni, I am
it Jataveda," he (the Fire-god)
replied. " What power resides
Agni replied: "I can burn
whatsoever exists on earth." asked:
BRAHMAN in thee?" up all
VI BRAHMAN and said: rushed towards
able to burn and it.
placed a straw before "Burn
it what him He (Agni)
speed, but was not So he returned from there
with all said (to the Devas):
find out this." this great
"I was not able mystery
is." toKena- Upanishad 1 09
VII said to Vayu (the Air-god)
out what this mystery they
THEN "Vayu! Find is." :
He said: "Yes." VIII HE
ran towards I am He (Brahman)
and "Who said to him:
am it art Matarisva Vayu,
Heaven)," he (Vayu) thou?" "I
(traveller of said. IX THEN
is the Brahman said: "What power
in thee?" blow away all
Vayu replied: "I can whatsoever exists on earth."
BRAHMAN and said: placed a straw before him
"Blow this away." He (Vayu) rushed towards it with all speed, but
was not able to blow it away. So he returned
from there and said (to the Devas): "I was
not able to find out what this great mystery
is."no The Upanishads XI said to Indra:
they THEN havan (Worshipful One)
! "O Mag- Find out
what this mystery is." He said: "Yes";
and ran towards it, but it disappeared before
him. XII he saw in that very space a woman
beautifully adorned, Uma of golden
THEN hue, daughter of Haimavat (Himalaya).
He asked: "What is this great mystery?"
T_IERE we to give see
how the Absolute assumes concrete form
knowledge of Himself to the earnest seeker.
Brahman, the impenetrable mystery, disappeared and in
His place appeared a personal form to represent Him.
is a subtle way of showing the difference between the
Absolute and the personal aspects of Deity. The Absolute
is declared to be unknowable and unthinkable, but He
This assumes make Himself known to
Thus Uma, daughter of the Himalaya, rep-
deified personal aspects to His devotees.
resents that personal aspect as the offspring of the Infinite
Being; while the Himalaya stands as the symbol of the
Eternal, Unchangeable One.part CHE (Uma)
^ through jFourtft "It said:
is Brahman. It is the victory of Brahman that ye
Then from her words, he
are victorious." (Indra) knew that
it (that mysterious form) was Brahman.
TMA replied to Indra, act.
He is It is to
Brahman that you owe through His power that you live
the agent and you are all only instruments
your victory. and " It is
His hands. Therefore your idea that 'This victory is
based on ignorance." At once
Indra saw their mistake. The Devas, being purled up
in ours, this glory is ours,' is
with vanity, had thought they themselves had achieved
the victory, whereas it was Brahman; for not even a blade
of grass can move without His command.
II these THEREFORE Vayu and Indra
Devas, Agni, excel other Devas,
because they came nearer to Brahman. It
was they who first knew this spirit as Brah-
man.The Upanishads ii2 III Indra
THEREFORE Devas, because excels other
all he came nearest to
Brahman, and because he first (before
others) knew this spirit as Brahman.
A GNI, Vayu and Indra were
all superior to the other
Devas because they gained a closer vision; and they
were able to do this because they were purer; while Indra
stands as the head of the Devas, because he realized the
Truth directly, he reached Brahman. The significance of
this is that whoever comes in direct touch with Brahman
or the Supreme is glorified.
IV THUS the teaching of
illustrated in Brahman regard to
the He is flashed like lightning, and appeared
disappeared just as the eye winks.
' A here Devas. and
HE teaching as regards the Devas was that Brahman
the only Doer. He had appeared before them in
a mysterious form; but the whole of the unfathomable
I is Brahman could not be seen in any definite form; so at
the moment of vanishing, He manifested more of His
immeasurable glory and fleetness of action
dazzling flash of light. by a suddenKena-Upanishad
113 V NEXT man (the teaching)
is (the embodied regarding Adhydt-
Him seems to approach The mind
Soul). (Brahman). By mind (the seeker) again and again
remembers and thinks about Brahman.
this /"\NLY by the mind can the seeker after knowledge
approach Brahman, whose nature in glory and speed
has been described as like unto a flash of lightning. Mind
alone can picture the indescribable Brahman; and mind
Him. alone, being swift in its nature, can follow
It is through the help of this mind that we can think and
meditate on Brahman; and when by constant thought
of Him mirror mind becomes
the can it reflect purified, then
His Divine Glory. like a polished
VI Brahman THAT (object of
worshipped by is Tadvanam called
He is to be adoration).
the name Tadvanam. He who knows Brahman
thus, is loved by all
beings. D RAHMAN all beings.
is the object of adoration and the goal of
this reason he should be worshipped
For and meditated upon as Tadvanam. Whoever knows Him
in this aspect becomes one with Him, and serves as a clear
channel through which the blessings of
to others. qualities The knower
and is of God therefore loved
Brahman partakes of by all
all flow out His lovable
true devotees.The Upanishads ii4 VII
r I ^HE A disciple asked:
O Master, teach me (The teacher replied :)
the Upanishad. The Upanishad has been taught
thee. We have certainly taught thee the Upanishad
about Brahman. VIII THE Upanishad
based on tapas (prac- is
tice of the control of
senses), karma actions). is its
dama (right body, mind and
(subjugation of the senses), performance
The Vedas are its of
prescribed limbs. Truth support. IX
HE who knows this (wisdom
of the ishad), having been cleansed of
Upan- all sin, becomes established in the blissful, eternal
and highest abode of Brahman, in the highest
abode of Brahman. Here ends
this Upanishad.Kena - Upanishad '~T"*HIS Upanishad is called
* with the inquiry: "By
whom comes and see? nition of
What life? And Kena, because
whom" mind go towards directed does the
115 its man enables it
begins (Kena) willed or From
object? to speak, to hear
him the the teacher in reply gives
Brahman, the Source and Basis
defi- of existence. The spirit of the Upanishads is always to show that no
matter where we look or what we see or feel in the
visible world, it all proceeds from one Source.
The prevailing note of all Vedic teaching is this: One
tremendous Whole becoming the world, and again the
world merging in that Whole. It also strives in various
to define that Source, ways
knowing which all known and without which no knowledge can be
tablished. So here the teacher
else That which replies: is
well es- is the eye of the eye, the ear of the ear, that is the inex-
haustible river of being which flows on eternally; while
bubbles of creation rise on the surface, live for a time,
then burst. The teacher, however, warns the disciple that this eye,
mind, can never perceive It; for It is that which
illumines speech and mind, which enables eye and ear
ear, "It is sense-faculties to perform their tasks.
from the known and also It is beyond the un-
known." He who thinks he knows It, knows It not;
because It is never known by those who believe that It
can be grasped by the intellect or by the senses; but It
and all distinct can be known by him
who knows It as the basis of all
consciousness. The knower of Truth
says, he realizes the unbounded,
"Thou "I know infinite art this (the visible),
Thou It not," because nature of the Supreme.
art That (the invisible), beyond," he declares. The ordi-
nary idea of knowledge is that which is based on sense-
and Thou art all that
is1 1 6 The Upanishads
preceptions; but the knowledge of an illumined Sage is
He has all the knowledge
not confined to his senses.
that comes from the senses and
The special purpose of this
all that comes from Spirit.
is to give us the
Upanishad knowledge of the Real, that we may not come under the
dominion of the ego by identifying ourselves with our
body, mind and senses. Mortals become mortals because
they fall under the sway of ego and depend on their own
limited physical and mental strength.
parable of the Devas and Brahman
real power, no real doer except God.
The is He lesson of the
that there is is no
the eye of the and all our facul-
Him. When we thus realize Him as the underlying Reality of our being, we
transcend death and become immortal.
eye, the ear of the ear;
ties and eyes, have no power independent
ears, of OM! PEACE! PEACE! PEACE!MUNDAKA UPANISHADFOREWORD
A S the present edition of the Upanishads
goes out to meet the need of aspiring students
and thinkers in the field of philosophy, we offer
a word of explanation. The first and second
editions of this book contained the translation
of three Upanishads namely, Isa, Katha and
Kena to which we now add a fourth called
Mundaka. The translator, Swami Paramananda, en-
dowed with a rare gift of penetration, inter-
preted the Sanskrit text in clear, simple lan-
Admirers of his first volume requested
guage. him Upanishads in his char-
manner, true to the original in spirit as
to translate other acteristic well as in poetic form.
fulfill the in this wish
in part. The Swtmi was
able to In 1920, he translated
Mundaka-Upanishad which was published Veda"nta Monthly, "Message of the
the East." Multiple activities prevented the contin-
uance of the task. The Swami wrote a short introduction for
Katha and Kena Upanishads but Mun-
the Isa, daka was published without his usual foreword.
Knowing full well that an explanatory corn-necessary to answer some of the
which questions invariably rise in the reader's
I taken the liberty of writing one.
have mind, mentary is Over a decade
ago, I had the great privilege
of studying the Upanishads in the original
Sanskrit under Swami Paramananda. He, like
the true illumined teacher, imparted to
essence of these lofty teachings in a
me the manner known to the initiates of the land where the
Forest-books were born. The experience was
more than that of learning the text or its mean-
ing. It was partaking of the light that streams
from the mind of the master to that of an
aspiring disciple. This was the beginning of a new era in
life for soon afterwards, I received
own my my ordination and commission to expound Ved^nta
from the Swami's platform. The early classes
on the theme of the Upanishads were conducted
A A under the open sky at Ananda Ashrama, Cali-
fornia. My first sermon-lecture was on "Ex-
altation of the Upanishads." Through long and
who was close association with one
whose learning was only a cloak for his inner realization who made the
truly wise ; ; letter of the Scriptures living
by his example, I dare hope that someday I may be used to
complete the work he had begun. As his stu-
nowdent and follower, I humbly pray that through.
His grace \Yho makes "The dumb to speak and
the lame to cross the mountain," I may prove
worthy of this task. GAYATRI DEVI
February, 1941 Boston, MassachusettsThe Mundaka-U panishad forms a part of the
Atharva-Veda. It has been called a Mantra-Upani-
shad as it is composed of verses in the form of Man-
tras or prayer-chants. Commentators observe that
these Mantras are not for the purpose of ceremonial
worship as are those of the Karma-Kanda or
portion of the Vedas. sacri-
ficial This Upanishad lays particular emphasis upon the
means of attaining Brahma-Vidya or knowledge of
the Absolute. The question is asked: "What is that,
by knowing vvhich everything else becomes
knozvnf" The sage answered that to acquire the High-
est Wisdom, one must transcend the vanity of lower
knowledge. Supreme Wisdom cannot be attained by
Sire, superficial study of the Scriptures, nor
by observing religious rites, nor by good works. It can only be
realized by the man of meditation
one ivho has been purified through
renunciation. the practice It is difficult to trace the
of discrimination and meaning of the
title "Mun- daka." The head." This may imply that the author of the book
literal translation of the word
zvas a Rishi or seer with shaven-head or
cate that the Upanishad essentials like the
itself is is it shorn of
''shaven- may indi- all non-
mind illumined by Brahma-Vidyti.MUNDAKA UPANISHAD
PEACE CHANT OM ! May we
which is hear with our ears that
beneficent, O Devas! May we
behold with our eyes that which is beneficent!
With a strong, well-poised body and worshipful
heart may we enjoy life and perform deeds
which are pleasing to the Deity.
OM! PEACE! PEACE! PEACE! i
Brahmzl was the first of the Devas, the
Creator of the universe, the Protector of
the world. He taught His oldest son Atharva
OM ! knowledge of Brahman (the Supreme)
which is the foundation of all knowledge.
the word THE Vedic Om
study is frequently found at the outset of
used in the sense of "Hail unto
Thee" or "Adoration unto the Supreme." Brahma is
the personal aspect of Deity as distinguished from
Brahman, the Absolute. He represents the creativeThe Upanishads
124 power of the universe. He is regarded as the fore-
most of all the various aspects of Divinity known as
Devas. The Devas (gods) in Vedic Scriptures are
Bright Beings, each manifesting some special Divine
quality and holding some special office in creation.
They correspond to the angels and archangels of
Semitic Scriptures. II knowledge THIS
ma taught to to of
Brahman which Brah- Atharva, Atharva taught
Angir Angir taught it to Satyavaha
Bharadwaja and Bharadwaja taught it in due
first ; ; succession to the sage Angiras.
Ill the great householder, having
SHAUNAKA, approached the sage Angiras with fitting
humility asked of him What is that,
Bhaga- O : van (revered Master), which -being known,
else becomes known? IT he
was customary in ancient India for a pupil,
all when sought instruction of a holy sage, to approach
him hearing in his arms a bundle of wood for the
altar fire. This armful of
sacrificial wood, called in Sanskrit Samit-f>ani, became the symbol of disciple-
ship, representing a desire to serve even in the hum-
Those ancient teachers did not give out
knowledge for a certain fee as it is done in the
blest capacity. theirMundaka-Upanishad modern educational
institution. 125 They were wholly
in- did not seek pupils. On the contrary,
they were very careful whom they taught. The pupil
was obliged to prove his worthiness by humility, earn-
dependent. estness They and patient
service. IV said to Sage
THE knowledge him There are two kinds
: of to be known, so are we told
by the knowers of Brahman, higher knowl-
edge and lower knowledge. V
knowledge consists of the Rig-
Veda, Yajur-Veda, Sama-Veda, Atharva- Veda, phonetics, ceremonial, grammar, etymol-
LOWER ogy, metre, astronomy. that
Higher knowledge by which the Imperishable is known.
is make a clear distinction between
(Afara) and knowledge born of direct vision (Para). Secular or lower knowledge
not only includes astronomy, ceremonial, rhetoric and
all branches of intellectual study, but even study of
the Sacred Scriptures. Any knowledge which is ac-
quired through study is classed as lower knowledge,
because merely reading or hearing about Truth is not
THE Upanishads knowledge secular The Aryan sages did not discredit
study of books, but they recognized that theo-
retical knowledge must always be inferior to knowl-
knowing Truth. the edge based on direct experience.The Upanishads
126 VI which cannot be seen, which cannot
be seized, which has no origin and no
THAT no eyes nor ears, no hands nor feet
That which is eternal, diversely manifesting,
attributes, ; all-pervading, extremely subtle that Imperish-
able One the wise regard as the Source of all
; created things. or higher knowledge is that which
by the senses or by our ordi-
PARA-VIDYA cannot be perceived nary faculties.
mind and It dawns intellect
in the soul only when
have become pacified and senses,
full of We do not gain ultimate realization until
we have subdued the turbulence and unrest of our
mind. At present the greater part of our effort at
knowledge is physical. We wish to see with our eyes,
grasp with our hands but through these channels we
can gain only objective knowledge; while knowledge
of God is subjective. This is evident from the defini-
serenity. ; tion of the
Supreme given here. He is
unconditioned and beyond the reach of our physical senses, our mind
and intellect. To perceive Him we must cultivate an-
other state of consciousness, which is done through
the practice of meditation. VII
AS the spider brings forth and
(its thread), as herbs spring
earth, as hair grows on the
draws in from the living body, like-Mundaka-Upanishad
127 wise does the universe come forth from the Im-
perishable. VIII Brahman expands produced from food
come Prana (energy), mind, the elements, the
worlds, good works and their immortal fruit.
Tapas THROUGH from food is
this, ; ; IX that one
Who all-perceiving and consists of wis-
is FROM all-knowing, Whose Tapas
dom, are born Brahma (the Creator), name,
form and food. signifies TAPAS
ously as means literally heat,
known The word spiritual discipline,
penance, austerity, and is etc.
employed vari- Tafia in this case be-
supposed to act on the sys-
tem like fire, consuming all impurities. This interpre-
tation, however, cannot be applied to the present text,
as it is evident that Brahman, the Supreme Lord, has
cause spiritual practice is no need of purification.
fire of wisdom, which like all
this come wisdom fire, It is
burning in used here to signify the
fire expands. Out of the
mind of Brahman, the creative power, and thence
forms of material manifestation. forth,
first alli the is Truth
the : sacrificial rites THIS
which the sages found in the hymns are
described variously in the three Vedas. Per-
form them is faithfully, O
ye Truth-seekers ; this the path that leads to the world of good deeds.
chapter deals in greater detail with the lower
THIS knowledge (Apard, Vidya} which according to
the previous chapter includes all forms of intellectual
study, ceremonial, ritual, etc. divided into two distinct parts.
Kdnda, deals with the philosophy
those who ; The Vedic teaching is
The one, called Gndna- subtlest
the other, called phases of
spiritual Karma-Kdnda, shows to cling to worldly things how by the
performance of certain rites and sacrifices they may
still attain the fulfillment of their desires.
The altar fire important part in these sacrifices, because
is regarded as one of the truest symbols of Divin-
It consumes all impurities without itself being
plays an fire ity. contaminated.
It has also a deeper significance.
The which every worshipper must light is the fire
of wisdom upon which every morning, noon and night
he must pour the oblation of his thoughts, words and
deeds. This lower knowledge is described at length
real fire in order to make plain to the disciple the perishable
Also to test fruits of all sacrifice.
nature of the whether his mind is wholly free from desire for
and ready for the higher knowledge.
earthly rewardsMundaka-Upanishad 129 II WHEN
the sacrificial fire the flames rise,
lations devoutly let a is
man kindled and offer his ob-
between the flames. Ill the fire sacrifice (Agnihotra) is not per-
at the new moon and the full moon,
IF formed during the autumn season and
and is fice, or at harvest time,
not attended by guests, or is without
offerings, or is without the Vaiswadeva sacri-
is junctions, offered contrary to the Scriptural in-
it will destroy the seven worlds of the
sacrificer. times and conditions, determined by the
the stars, planets or satellites, by the
CERTAIN position of season, by the attendant circumstances, were regarded
as peculiarly auspicious for performing sacrifice. If
these were not observed, then the sacrifice
ered barren and the sacrificer lost
all was consid- the benefits to
be derived in the seven worlds from his sacrifice.
The seven worlds represent the rising grades of heavenly
find the same idea in the Western ex-
pleasure. We pression "seventh heaven."
IV K ALI (dark), Karali
(terrific), Mano- java (swift as thought), Sulohita (veryThe Upanishads
130 Sudhumravarna (deep purple), Sphulin-
red), Viswaruchi (universal light) are the seven flaming tongues of fire.
gini (sparkling), V a man performs
IF season his sacrifice in the proper
and pours out his oblations
on the shining flames, these oblations like the rays of
the sun lead him to where the Supreme Lord of
sacrifice dwells. VI COME hither!
Come hither! the bright ob-
lations say to the sacrificer
and carry him by the rays of the sun while with pleasing
words they praise him, saying: This is the
heavenly Brahma-world (Svarga) which thou
; hast earned by thy good deeds.
VII BUT all these sacrifices
(performed by) eighteen are inferior and ephemeral. The
ignorant who regard them as the highest good
and delight in them, again and again come
under the dominion of old age and death.
the sacrifices sixteen priests were supposed to
part, together with the sacrificer and his wife,
IN take making up the eighteen mentioned in the verse. Ac-
cording to Vedic injunctions no householder's sacri-Mundaka-Upanishad
fice was 131 fruitful unless his wife took part in
it with him. VIII dwelling in ignorance, yet imagin-
wise and learned, go round
FOOLS ing themselves and round
in devious ways, afflicted by many
troubles, like the blind led by the blind.
same verse appears with a slight variation
in the Katha-Upanishad (Part II, Verse V) and
THIS is fully commentated mere
there. the danger of Here
it seeks to emphasize fos-
intellectual learning which, tering a man's pride, leads
him to believe that he is
capable of guiding others, even in spiritual matters,
although he himself is devoid of spiritual understand-
ing. He wishes to be a leader, but in the Vedas it is
no one save the man of direct
insistently taught that vision shall venture to lead others.
IX (the unawakened), CHILDREN different ways overpowered
in many by ignorance, imagine that they have achieved their aims.
These performers of Karma (sacrifice),
be- cause of their attachment to the fruits (of their
sacrifice), after a heavenly reward
temporary enjoyment of their back again into misery.
fall X R EGARDING sacrifice
and good works as the highest aim, these ignorant
men knowThe Upanishads 132 not the higher goal
and ; after having enjoyed
the heavenly pleasures earned by their good
deeds, they return to this world or fall into a
lower one. whose sphere of
THEY plane this believe that
vision is wholly limited to
when they have achieved a
certain success in the world, they have gained all that
and they bind themselves with
vanity and egoism. Even when the great
to be gained there is
their own ; Saviours come and strive to awaken them, they
own cling stubbornly to their
point of view. this in the life of Jesus the Christ.
who knew The We still
see scholars, those the letter of the law and regarded them-
selves to be wise, Whatever
a were the man last to
earns by accept His message.
finite actions, however good, cannot be permanent so all heavenly pleasure
must come to an end and he must return once more to
; the plane of struggle
BUT those wise and discipline.
XI men of tranquil heart,
who Shraddha (faith) and Tapasya
(austerity) in the forest, living on alms, free
from all impurities, travel by the path of the
sun to where the immortal, imperishable Being
practice dwells. XII a LET
ing Brahmana (God-seeker), examined all
these after hav- words attainedMundaka-Upanishad
Karma-Marga through (sacrifices 133 and good
deeds), become free from all desires; realizing
that the Eternal cannot be gained by the non-
In order to acquire knowledge (of the
eternal. him then, with sacrificial fuel in
Eternal) let his hand, approach a Guru (spiritual teacher)
who is well-versed in the Vedas (Scriptures)
is establishd in Brahman (the Su-
and who preme). XIII him who has thus approached rever-
ently, whose heart is tranquillized, and
whose senses are under control, let the wise
Guru teach the real knowledge of Brahman,
by which the true and immortal Being is known.
TOSECOND MUNDAKA Part Jtrut i
the truth. As from the blazing fire
forth thousands of sparks like unto
so also, gentle youth, do the various beings
is THIS burst fire, spring forth from the Imperishable and re-
turn thither again. ALL the
reunited tion things have their origin in the Supreme, and
ultimate aim of all life and effort is to be
with the may seem Source.
The play of manifesta- to block the consciousness of the
un- derlying link between' the human and the Divine, but
sooner or later all souls must regain that conscious-
ness. II effulgent Being is
without form THAT both without and within
exists born ; ; He
without breath and without mind
; is ; He un-
pure, higher than the High Imperishable.
is the definition of the Absolute, the
in the Vedic terminology given
HERE Unconditioned, known as Nirgitna-Brahinan.
ated, therefore He must The Absolute cannot be
be without form ; because
cre- He all-pervading, therefore He must be within
and without all things. For the same reason He does
is infinite,Mundaka-Upanishad 135 not breathe, nor has He need of the instrument of
mind for thought. The High Imperishable here re-
fers to the Creative Energy, what is known as Saguna-
Brahman, that God personal man without
; is, Brahman with attributes,
or the while Nirguna- Brahman means Brah-
attributes. Ill FROM mind, Him
are born the Prjhia (life-force),
the sense organs, ether, air,
and the earth, support of all.
water all fire, IV His head, sun and moon are His
the four quarters are His ears, the
eyes, revealed Vedas are His words, His breath is
is FIRE His heart is
came forth the the air, the universe,
feet earth. of all He
is and from His the inner Self
living beings. V Him comes
FROM sun the (rain) ;
the fire, Whose fuel is
from the moon come the clouds
from the earth come all herbs the male
; ; places the seed in the female, thus
are born many beings from the Purusha (the Great Being).
VI Him come FROM jur (Vedas),
the Rik, the rites Saman and Ya-
of initiation, all forms of sacrifice, special ceremonials, sacrifi-
cial gifts (to the priests), the appointed seasonThe Upanishads
136 (for sacrifice), the sacrificer, and
which the moon sanctifies all
the worlds and the sun
illu- mines. VII FROM Him
are the various Devas'born, the
Sadhyas (Genii), men, beasts, birds, the up-
breath and the down-breath, corn and barley,
austerity, faith, truth, continence, and (Scrip-
tural) injunction. VIII Him are
born the seven FROM (senses), the seven
Pranas lights (of sense-percep- tion), the seven fuels (objects of perception),
the seven oblations (acts of sense-perception),
and the seven Lokas (seats) where the senses
move seven ; in each living being, residing in the
heart. IX Him FROM and divers
are born all oceans, mountains
From Him come rivers. all
herbs and juices, by which the inner self sub-
sists, together with the gross elements.
X I HAT Being alone
austerity. All is is all
this Brahman, sacrifice the and
HighestMundaka-Upanishad these verses IN creation
we 137 are given a picture of cosmic
Brahman, the Supreme. The rising out of
first manifestation and heat. is
Fire (Agni), the giver of light
Heat brings forth life ;
rain falling upon the earth causes vegetation to spring up, thus food is
produced from food comes the procreative energy.
All aspects of being gods, genii, men and beasts are
; Him. The Scriptures and all
Every Him. directly connected with
rites and ceremonies have sprung from Him.
form has its origin in
The seven senses referred to in Verse VIII are the
two eyes, two ears, two nostrils and mouth. The
seven lokas or seats represent the avenues of sense
The inner self in Verse IX signifies the
body made up of the mind (manas), intellect
(buddhi) and ego (ahamkara). He who realizes the
all-pervading and eternal Cause of Creation as abid-
perception. subtle ing in his
own heart, the Life of his life, attains illu-
mination even here in this body.
He who knows this (Being) dwell-
cave of the heart, O gentle youth, cuts
asunder even here the knot of ignorance.
Immortal. ing in theSECOND MUNDAKA
i well-seated in the heart,
in the heart, SHINING, port of
all is moves, breathes and winks.
both being and non-being, human
reach of moving the Great Being, the Sup-
Him In all. is fixed,
whatsoever Know Him Who Who
is beyond is the understanding, the highest and
most adorable One. Supreme Lord abides
THE from Him in every heart and
alone springs all activity. Nothing can
exist apart from Him. In Him the whole universe is
centered. He is both formful and formless. He is
present in all the forms we see, yet we cannot lay our
hands on any form and say, "This is God." He is both
personal and impersonal. He is the manifested and
the unmanifested (being and non-being).
He is the final goal of
all effort. II which is effulgent, subtler than the
on which all worlds and those
who dwell in them rest, that is the imperishable
THAT subtlest, Brahman ; that
is and mind. That tal.
That mark youth ! PrSna (breath), that
is is the true, that
to be hit. is Hit
is the it, speech Immor-
O gentleMundaka-Upanishad 139 III taken
fixed in votion fixed ;
the up HAVING great weapon,
as the the Upanishad, bow
and having ; the arrow, sharpened by steadfast de-
then having drawn it with the mind
it on the Supreme, perishable,
O hit that gentle youth
mark the Im- ! IV
THE man word sacred (Self)
is Om is the bow, the At-
the arrow, Brahman (the That mark
should be hit by one who is watchful and self-
possessed. Then as the arrow becomes one
with the mark, so will he become one with the
Supreme) said to be the mark.
is Supreme. WE have here a poetic and archaic picture of the
The study of process of realizing the Supreme.
the Upanishads is ahstract. They deal with the In-
finite. But the sages who give the teaching contained
them try by similes to relate these abstract truths
life here. The aim of all our striving is the
Absolute and Eternal careful study of the Scriptures,
constant practice of meditation, and untiring devotion
serve as the means by which we attain it.
in with our : In the next verse the teacher makes
definite. ginning, Om, is the
Logos or Word that taken as the bow, the soul of
arrow and the Infinite is the
mark. still it was To
more in the be- man
hit the is the markThe Upanishads
140 the mind must be wholly
An collected. illustration of MahSbhSrata. Arjuna and his kins-
men, it is told, were called to a contest in archery. The
target was the eye of a fish raised on a high pole.
Their teacher asked each one in turn, "What do you
see?" They all described the whole fish. But when
this is given in the
Arjuna was asked, he replied: "I see only the eye of
the fish"; and he alone hit it. If our mind is divided
or scattered we cannot meditate
and without the ; power of meditation we cannot gain
of the Imperishable. When we
direct perception do gain this perception
what happens? We become one with the Supreme.
The knower of God partakes of His nature.
V Him are fixed the heaven, the earth, the
IN sky and the mind with all
Him and abandon to be the Self of
words. He is all, Him
all vain the bridge to immortality.
can be no form of manifested
THERE from Him. Know the senses.
life apart When we understand this and realize
as the essence of our being, we lose attachment
for mortal things and cease to lay so much stress on
lower knowledge. "Vain words" here signifies theo-
retical speculation, book knowledge, everything in fact
which fosters our egotism or pride and does not lead
to ultimate Truth. Thus knowledge of God serves as
we cross from the mortal to the
the bridge over which immortal.Mundaka-Upanishad
141 VI WHERE the nerves of the
body meet to- gether as the spokes in the nave of a
wheel, here the Atman dwells, variously mani-
Meditate upon that fested. Atman
as Om. May there be no obstacle in thy crossing to the other
side, of darkness! THE seat
of this Divine Principle in us
This inner Principle or Soul
heart. is in is the
Itself unchanging, but It appears to take the modifications
of the mind, such as joy, grief, anger, jealousy, hatred,
is love, etc. Thus It manifests variously. Since
Om the ultimate Name Supreme and Atman
of the is Supreme, by meditating on Om
as the Atman we direct our thoughts toward the
highest and pass beyond the darkness of ignorance.
identical with the VII Who is
to Whom HE all-knowing and all-perceiving,
belongs all the glory of the uni-
verse, that Self dwells in the heavenly city of
Brahman (the heart). VIII HE
takes the form of mind and becomes
the ruler of the body and the senses. Be-
ing in the heart, He sustains the body by food.
The wise, forth, who realize this, behold
immortal and all-blissful. Him shiningThe Upanishads
142 IX is seen, Who is both high and
low, the fetters of the heart are broken,
WHEN He and doubts are cut asunder,
all of (bondage WHEN work)
is all Karma destroyed. the vision of
Him, Who is all there
is, high or low, subtle or gross, vast or small,
dawns within us, the heart is at once freed from the
fetters of attachment, selfishness ;
all the egotism and every form of
and doubts of the perplexities
mind are cleared away. This does not happen when
we gain intellectual knowledge only. The more we
read and analyze, the more confused and entangled
the mind becomes. But when we behold God directly,
once all the darkness of doubt disappears in the
glory of His self-effulgent light, as the night goes
when the morning comes. That brightness no one can
at define. to us, in
As long as we need someone to prove Truth
we have not found it. But when His light shines
our heart, Truth becomes self-evident. Without
light the outside world will always be full of
this shadows for us but when that light is found, the
whole universe glows with its radiance. Then all the
chains of Karma past, present and future are shat-
tered, and the soul enjoys perfect freedom.
; X THAT Light stainless indivisible
of all lights, Brahman, pure,
dwells in the innermost golden sheath (the core of the heart). Thus do
the knowers of Self know Him.Mundaka-Upanishad
143 XI THE sun does not shine there, nor the moon,
stars, nor do these lightnings shine
nor the there, much less this fire.
everything shines after is Him
When He ; by His
shines, light all lighted. T
HIS same verse also Upanishad, Part V,
in appears the Katha- v. 15.
XII immortal Brahman Brahman THAT
is before, that Brahman is to the
and to the that Brahman
extends left; right above and below. The Supreme Brahman alone
is is behind, that the whole Universe.THIRD MUNDAKA
fart 3Ftral i inseparable companions of golden
plumage perch on the same tree. One of
them eats the pleasing fruit (of the tree), the
other looks on as a witness without eating.
TWO II ON the same
tree (of life) man sits,
his own in grief, drowned
helpless- overpowered by But when he beholds the other, the Lord,
majestic and full of glory, then his grief passes
ness. away. two birds represent the higher Self and the
THE lower self. The lower
self is absorbed in tasting
life, and imagines cannot escape from the reactions caused by them
When, however, in its struggle it looks up to that
other transcendent Self and perceives how alike they
are, it realizes its true nature. The Jiva or individual
the sweet and bitter fruits of this
it soul is merely the reflex of the Paratndtman or Su-
preme Soul. The apparent man has its root in the
real man. As soon as we realize this, the two become
one. It is the sense of ego in us which divides and
separates and whenever we separate ourselves from
; our Divine part, we feel a lack. When, however, we
discover our relation with the inexhaustible Source, allMundaka-Upanishad
our to selfish appetites 145
which now drive us from branch
life will vanish and no cause
branch of the tree of
for grief will remain. Ill
the sun perceives that Being of
golden radiance, the Creator, the Lord,
the Source of Brahman (creative power), then
WHEN that knower, having cast
and being off all sin
and merit, stainless, attains the highest
oneness (with the Supreme). is an iron
which binds us through self -righteousness and love of name and
fame. To gain ultimate union, one must transcend the
consciousness of both sin and merit.
the IN chain, Indo-Aryan conception, as
sin so merit can be a golden chain,
IV HE is the Prana (life-force) animating
living beings. He who knows
all this be- comes truly wise and not merely a talker. He
delights in the Self (Atman), he finds his high-
est happiness in the Self, and he is a true per-
former of duty. the Verily he
is the foremost of knowers of Brahman (the Supreme).
a man beholds God, he gains true wis-
and no longer finds satisfaction in vain dis-
cussion and speculation. His joy and recreation are
WHEN domThe Upanishads 146 found
in the Infinite. Because his mind and
completely unified with the cosmic mind and
fulfills the law spontaneously and hence never
his duty. will are will,
he fails in V pure and effulgent Self, which dwells
within the THIS body and is realized by sinless
Sannyasins (the spiritually consecrated), can
be attained by truthfulness, self-subjugation,
true knowledge and the steadfast practice of
chastity. VI TRUTH Truth path by
alone conquers, not untruth. By
the spiritual path is widened, that
which the Seers, who are free from all
desires, travel to the highest
abode of Truth. VII and
THAT shines forth, immeasurable, divine
inconceivable, subtler than the subtlest,
more distant than the distant, yet here (in the
body). Residing in the cave of the heart, so It
is seen by true Seers.
VIII HE not perceived by the eye, nor by
speech, nor by the other senses, nor by
is austerities, deeds) ; nor by
when the Karma mind is
(sacrifice and good purified by the sereneMundaka-Upanishad
147 knowledge, then alone does the Seer
light of perceive the indivisible
meditation. Brahman by means of
IX subtle Self THIS heart
as seated is to be realized by a pure
where the PransL there (life-force) has entered in five-fold form.
mind of every creature When
senses. forth of it is
is The interwoven with the
purified, then the Self shines
itself. forms through which the vital
the body are Prana, Apana
Samana, Vyctna and Udhdna. These represent in-
five different THE energy manifests
in breathing, out-breathing, equalizing the breath, circu-
lating the breath, and up-breathing. By these various
actions of the vital energy different nerve currents an
This has been elaborately worked out it
system. Prana governs respiration Apana, the organs of excretion Samana, digestion
Vyana, the general nerve currents of the body, and
Udhana, speech. The senses also are often spoken of
controlled. the Indian Yoga ;
; the Upanishads as Prtinas, because they are the
avenues through which the vital energy connects the
outer world with the inner. The sense impressions
in gained through these channels color all our mental
activities and not until the mind is freed from these
; obscuring impressions can the soul manifest
nature. its trueThe Upanishads 148
X WHATEVER mind desires, jects.
worlds the man of purified
covets, and whatever objects he
he obtains those worlds and those ob-
let Therefore, spiritual welfare, the
man who longs for his
worship that one who knows
the Self. WHEN a man's
only what in mind is
purified, he realizes his oneness with the cosmic
fore, his is harmony with
desires course of law. fulfill
who and themselves is, desire
will ; there- by the natural
Purity of mind by "worshipping," that
those life the cosmic will
is most quickly gained by revering and serving
possess the higher knowledge.THIRD MUNDAKA
i HE (the Seer of Truth)
knows the high- abode of Brahman, in which all this
universe rests and which shines with pure radi-
ance. Discerning men, without desire, by serv-
est knower ing reverently such a
yond the seed. THE Seer
who (of Self) go be-
has realized the Supreme and has
united himself with the Source of knowledge, be-
comes a connecting link between God and the wor-
Whatever homage or reverence is paid to
him, he does not take for himself. Being entirely free
from egotism and self-importance, he offers it all to
shipper. God. Those who seek out and serve such an illumined
soul gradually partake of his wisdom and pass be-
yond the need of birth and death. Every desire is a
seed from which spring birth, death and all mortal af-
Illumination alone will destroy this seed.
flictions. II HE who broods on objects of desire and
is born here and there ac-
covets them, cording to his desires but he whose desires are
fulfilled and who has known the Self, his de-
; sires vanish even here.The Upanishads
150 A SELFISH clings to
man, who the is identified with the flesh,
small and finite and however
; covetous of a larger life he may be, he cannot attain
A man may wish to go to the other shore but if
it. he does not pull up the anchor, his boat will not move.
; Ill Self cannot be attained by the study
of the Scriptures, nor by intellectual per-
THIS nor by frequent hearing of
the Self chooses, by him alone
ception, whom tained. To him
He It. is It at-
the Self reveals Its true nature.
same verse appears in the Katha-Upani-
(Part II, V. 23) and is explained at length
there. Only he whose heart is wholly purified and
made ready can receive the revelation therefore the
Self naturally chooses that one and no other. This
shad THIS ; means that we have to give ourselves wholly before
can get the higher vision.
we IV Self cannot be attained
THIS devoid of by one who
strength, or by one who
is is un- mindful, or by one whose austerity is without
renunciation. But if the wise man strives by
these means, his Self enters into the abode of
Brahman. THE Upanishads lay frequent emphasis on the
weak person can attain Truth but
idea that no ;Mundaka-U panishad
does not mean mere physical
strength required for spiritual vision
this 151 weakness. The an inner vigor.
The sages in choosing their disciples were careful to
choose those who were full of energy, faithful and
willing to do anything. Even arduous practice of aus-
terity, however, will be unfruitful, unless the heart is
freed from lower desires. THE
is Rishis (wise Seers), after having at-
It, become satisfied through knowl-
Having accomplished their end and being
from all desire, they become tranquil. The
tained edge. free self-possessed wise ones, realizing the all-per-
vading Spirit present in all things, enter into all.
THEY enter into all because they realize the uni-
life. They "see the Self
versal oneness of cosmic in all beings
and all beings in the Self." (Bhagavad-
Gitl) VI (spiritually SANNYASINS seekers), having
tainty the true apprehended with cer-
meaning Vedzmta, having consecrated of the
knowledge of by the purified their nature
practice of renunciation, and having realized the
highest immortality, after the great end (death)
become liberated in the world of Brahman.The Upanishads
152 VII fifteen parts return to their source
THEIR the senses go back
all ; to their correspond-
ing deities; the Self, together with his deeds
and acquired knowledge, becomes one with the
highest imperishable Brahman. 'T~ 1
HE fifteen parts referred to are
JL faith, ether, air, Prana
(life), water, earth, senses, mind,
fire, food, vigor, austerity, mantras (holy texts), sacrifice
and the worlds (of name and form). Faith is men-
tioned after PrSna because it is the greatest impelling
power in life. When the final realization comes, the
various parts of man's physical, intellectual and moral
being are blended into one harmonious whole and be-
come united with the Supreme.
VIII AS flowing rivers Ji\.
lose ocean, giving up the knower, freed from
the highest effulgent A 5
themselves name and form, name and form,
in the so also attains
Purusha (Being). man's consciousness expands into the universal
consciousness, the limitations of self -conscious-
ness necessarily melt away but he does not lose his
true entity. As soon as he attains knowledge of his
; true Self, he transcends the realm of name and form
and enters into conscious union with the universal
Source of existence and knowledge. As
it is said in theMundaka-U panishad
Prasna-Upanishad : 153 "He becomes without
parts and Brahman be- immortal."
IX who knows HE that highest
comes like unto Brahman. In his family
no one is born who is ignorant of Brahman. He
overcomes grief he overcomes sin and being
freed from the knots of the heart, he becomes
; ; immortal. declared in the following text
knowledge of Brahman be taught
is it THUS Let this
. to those only who have
enjoined in the Scriptures in the
who Vedas ; who performed
; who sacrifices are well versed
are devoted to Brahman, with faith have performed the fire sacrifice
and who have fulfilled the vow of
Ekarshi carrying ; fire on
their head (Shirovrata). signifies that only those who have
themselves by performing with proper
humility and devotion the various rites and vows
given in the Scriptures, will be able to understand or
verse THIS purified follow the higher knowledge. Therefore to them alone
it be taught. To others it will bring only con-
should fusion of mind and impede
their progress.The Upanishads 154 XI
THE sage Angiras in ancient times taught
Saunaka). It should not be
this truth (to studied by one who has not fulfilled the vow
of self-sacrifice and service. Adoration to the
great Rishis ! Adoration to the great Rishis
Here ends the Mundaka-Upanishad. OM! PEACE! PEACE! PEACE!
