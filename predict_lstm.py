import torch
from lstm import CharRNN, one_hot_encode
import numpy as np
import torch.nn.functional as F
import random

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

# Defining a method to generate the next character
def predict(net, char, h=None, top_k=None):
        ''' Given a character, predict the next character.
            Returns the predicted character and the hidden state.
        '''

        index = 0
        # tensor inputs
        if not char in net.char2int:
            index = random.choice(list(net.char2int.values()))
            print(index)
        else:
            index = net.char2int[char]
        x = np.array([[index]])
        x = one_hot_encode(x, net.n_fc)
        inputs = torch.from_numpy(x).to(device)

        # detach hidden state from history
        h = tuple([each.data for each in h])
        # get the output of the model
        out, h = net(inputs, h)

        # get the character probabilities
        p = F.softmax(out, dim=1).data

        # get top characters
        if top_k is None:
            top_ch = np.arange(len(net.chars))
        else:
            p, top_ch = p.topk(top_k)
            top_ch = top_ch.cpu().numpy().squeeze()

        # select the likely next character with some element of randomness
        p = p.cpu().numpy().squeeze()
        char = np.random.choice(top_ch, p=p/p.sum())

        # return the encoded value of the predicted char and the hidden state
        return net.int2char[char], h

# Declaring a method to generate new text
def sample(net, size, prime='The', top_k=None):

    prime = prime.encode()
    # First off, run through the prime characters
    chars = [ch for ch in prime]
    h = net.init_hidden(1)
    for ch in prime:
        char, h = predict(net, ch, h, top_k=top_k)

    chars.append(char)

    # Now pass in the previous character and get a new one
    for ii in range(size):
        char, h = predict(net, chars[-1], h, top_k=top_k)
        chars.append(char)

    return ''.join([chr(char) for char in chars])[len(prime):]

def init(text):
    chars = tuple(set(text))
    print(chars)
    print()
    pretrained = torch.load("rnn_20_epoch.net", map_location=device)
    model = CharRNN(chars, pretrained['n_hidden'], pretrained['n_fc'], pretrained['n_layers'], token_dict=pretrained)
    model.load_state_dict(pretrained['state_dict'])
    model.to(device)
    model.eval()
    print(model.char2int)
    return model

def main():
    with open('new_data/train.to', 'r') as f:
        text = f.read().encode()
    model = init(text)
    print(sample(model, 1000, prime='threads', top_k=5))

if __name__ == '__main__':
    main()
