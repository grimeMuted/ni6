import os
import re


max_size = 1000000

def find_messages_from_user(user, data_file):
    data_size = 0
    with open(data_file) as f:
        messages = ""
        is_message = False
        # Find messages from this user
        for line in f.readlines():
            if is_message:
                # messages starting with brackets will be ignored, but that's fine
                if line.startswith("["):
                    if not user in line:
                        is_message = False
                elif not (line.startswith("\n") or line.startswith("\r")):
                    messages += line
                    data_size += 1
            else:
                is_message = is_from_user(user, line)
            if data_size >= max_size:
                break
    prompts = ""
    is_message = False
    # Find the messages this user was replying to
    i = 0
    for line in reverse_readline(data_file):
        line = line.decode("utf-8")
        if is_message:
            if line.startswith("["):
                is_message = False
            elif not (line.startswith("\n") or line.startswith("\r")):
                prompts = line + "\n" + prompts
                i += 1
        else:
            is_message = is_from_user(user, line)
        if i >= data_size:
            break

    return messages, prompts, data_size

def is_from_user(user, line):
    return line.startswith("[") and user in line

def reverse_readline(filename, buf_size=8192):
    """A generator that returns the lines of a file in reverse order"""
    with open(filename) as fh:
        segment = None
        offset = 0
        fh.seek(0, os.SEEK_END)
        file_size = remaining_size = fh.tell()
        while remaining_size > 0:
            offset = min(file_size, offset + buf_size)
            fh.seek(file_size - offset)
            try:
                buffer = fh.read(min(remaining_size, buf_size)).encode("utf-8")
            except:
                buffer = b'\n'
            remaining_size -= buf_size
            lines = buffer.split(b'\n')
            # The first line of the buffer is probably not a complete line so
            # we'll save it and append it to the last line of the next buffer
            # we read
            if segment is not None:
                # If the previous chunk starts right from the beginning of line
                # do not concat the segment to the last line of new chunk.
                # Instead, yield the segment first
                if buffer[-1] != b'\n':
                    lines[-1] += segment
                else:
                    yield segment
            segment = lines[0]
            for index in range(len(lines) - 1, 0, -1):
                if lines[index]:
                    yield lines[index]
        # Don't yield None if the file was empty
        if segment is not None:
            yield segment


#user = "grimeMuted#1621"
#user = "Edza#0255"
#build_training_data(user)

def build_training_data(user, data_file="new_data/combined.txt"):
    alpha_user = re.sub(r'[^a-zA-Z0-9_]', '', user)
    messages, prompts, data_size = find_messages_from_user(user, data_file)
    print("Found " + str(data_size) + " lines of messages to create a model.")
    from_file = "new_data/train.from"
    to_file = "new_data/train.to"
    with open(from_file, "w") as f:
        f.write(prompts)
    with open(to_file, "w") as f:
        f.write(messages)
    model_folder = "model_" + alpha_user + "/"
    data_folder = "data_" + alpha_user + "/"
    return data_size, model_folder, data_folder

#user = "☀ Mittens ☀#3054"
#data_file="new_data/combined_update.txt"
#data_file="new_data/bunker.txt"
user = "✿ Mittens ✿#3054"
data_file = "new_data/mittens_zone.txt"

if __name__=='__main__':
    build_training_data(user, data_file=data_file)
