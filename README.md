# ni6

See this post to install Tensorflow with the Nvidia 390.77 Linux driver: https://medium.com/@asmello/how-to-install-tensorflow-cuda-9-1-into-ubuntu-18-04-b645e769f01d

On an Optimus laptop, run with `optirun python ni6.py`

Authorize the bot for a discord server: https://discordapp.com/oauth2/authorize?client\_id=513226159530704896&scope=bot&permissions=3072

Built using:

https://github.com/daniel-kukiela/nmt-chatbot

https://github.com/mkonicek/nlp

T. Mikolov, E. Grave, P. Bojanowski, C. Puhrsch, A. Joulin. Advances in Pre-Training Distributed Word Representations


